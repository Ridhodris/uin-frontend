import React from 'react'
import { Link } from 'react-router-dom'

export default function Settings() {
    return (
        <div className="flex flex-col justify-center items-center p-10 min-h-screen">
            <h1>Settings</h1>
            <Link to="/home" className="hover:underline">Logout</Link>
        </div>
    )
}
