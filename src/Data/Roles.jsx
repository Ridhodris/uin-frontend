export const rolesData = [
    {
        roleCode : 'r001',
        roleName : 'Dosen',
    },
    {
        roleCode : 'r002',
        roleName : 'Mahasiswa',
    },
    {
        roleCode : 'r003',
        roleName : 'Rektor',
    },
    {
        roleCode : 'r004',
        roleName : 'Public',
    },
]