import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,Redirect
} from "react-router-dom";
import Loading from './Components/Loading'

const CheckYourEmail = React.lazy(()=>import('./Pages/CheckYourEmail'));
const Oauth = React.lazy(()=>import('./Pages/Oauth'));
const ResetPassword = React.lazy(()=>import('./Pages/ResetPassword'));
const Login = React.lazy(()=>import('./Pages/Login'));
const ForgotPassword = React.lazy(()=>import('./Pages/ForgotPassword'));
const Fe = React.lazy(()=>import('./Pages/Index'));
const Dashboard = React.lazy(()=>import('./Pages/Dashboard'));

function App() {
  const [isUserAuthenticated] = useState(false)
  return (
    <div  className="App font-Poppins">
      <React.Suspense fallback={
                   <div className="h-screen absolute inset-0 flex justify-center items-center w-full">
                       <Loading/>
                   </div>
               }>
        <Router>
          <Switch>
          <Route
              exact
              path="/"
              render={() => {
                  return (
                    isUserAuthenticated ? <Redirect to="/fe" /> : <Redirect to="/login" /> 
                  )
              }}
            />
            <Route exact path="/login" component={Login} />
            <Route exact path="/forgotpassword" component={ForgotPassword} />
            <Route exact path="/resetpassword/:token?" component={ResetPassword} />
            <Route exact path="/checkyouremail" component={CheckYourEmail} />
            <Route exact path="/oauth" component={Oauth} />
            <Route path="/fe">
              <Redirect from="fe" to="/fe/admin" />
              <Fe/>
            </Route>
            <Route path="/dashboard">
              <Redirect from="dashboard" to="/dashboard/home" />
              <Dashboard/>
            </Route>
          </Switch>
        </Router>
      </React.Suspense>
    </div>
  );
}

export default App;
