import React from 'react'
import Loader from 'react-loader-spinner'

export default function Loading({visible}) {
    return (
        <div className={`absolute z-50 inset-0 flex items-center justify-center bg-white bg-opacity-75 ${visible ? '' :'hidden'}`}>
            <Loader
                type="Puff"
                color="#029cc3"
                height={100}
                width={100}
            />
        </div>
    )
}
