import React from 'react'
import { FiCloudOff, FiEdit,  FiSearch,  FiTrash, FiUsers } from 'react-icons/fi';
import MyModal from "../../../Components/MyModal"
import Input from "../../../Components/Input"
import Button from "../../../Components/Button"
import Loading from '../../../Components/Loading';
import {debounce} from 'lodash';
import Pagination from '../../../Components/Pagination';
import MySelect from '../../../Components/MySelect';
const axios = require('axios');

export default class Groups extends React.Component {
    state = {
        modalIsOpen  : false,
        loading      : false,
        alert        : false,
        AllGroups     : [],
        dataUsers     : [],
        filter       : {
            search   : ''
        },
        paginate : {
            perPage  : 10,
            page     : 0,
            lastPage : 0,
            pageCount: 0,
        },
        data         : {
            id       : '',
            groupCode: '',
            groupName: '',
            userList: [],
        }
    };
    
    componentDidMount(){
        Promise.all([this.getUser(),this.getData()])
    }

    handleChange = (e) =>{
        this.setState({
            data : {
                ...this.state.data,
                [e.target.name] : e.target.value
            }
        })
    }

    handleFilter = debounce((search) => {
        this.setState({
            filter : {
                search : search
            }
        },()=>{
            this.getData()
        })
    },500)

    getData = async () => {
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}group?search=${this.state.filter.search}&page=${this.state.paginate.page}&size=${this.state.paginate.perPage}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            this.setState({
                AllGroups : response.list,
                modalIsOpen : false,
                loading : false,
                paginate : {
                    ...this.state.paginate,
                    page : response.pageable.page,
                    lastPage : response.pageable.totalPages,
                    pageCount:response.pageable.totalPages,
                }
            })
        }).catch(err=>{
            console.log(err)
            this.setState({loading:false})
        });
    }

    handleAdd = () =>{
        this.setState({
            modalIsOpen : true,
            data : {
                id : '',
                groupCode : '',
                groupName : '',
                userList: []
            }
        })
    }

    handleDelete = async (id) =>{
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}group/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(()=>{
            this.setState({
                paginate : {
                    ...this.state.paginate,
                    page : 0
                }
            },()=>{
                this.getData()
                this.setState({
                    loading:false,
                    alert:false
                })
            })
        }).catch(err=>{
            this.setState({loading:false})
            console.log(err)
        });
    }

    handleEdit = async (id) => {
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}group/${id}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then( async response=>{
            let users = []
            this.state.dataUsers.map((e)=>{
                return response.users.map((a)=>{
                    return e.value === a.userUniqueId && users.push(e)
                })
            })
            // await Promise.all(response.users.map((e)=>users.push({value:e.userUniqueId,label:e.userUniqueId})))
            this.setState({
                modalIsOpen : true,
                loading : false,
                data : {
                    id : response.id,
                    groupCode : response.groupCode,
                    groupName : response.groupName,
                    userList: users
                }
            })
        }).catch(err=>{
            console.log(err.response)
            this.setState({loading:false})
        });

    }

    handleSelectedChange = (newValue, selectedOptions) => {
        if (selectedOptions.action === "clear") {
            this.setState({
                data:{
                    ...this.state.data,
                    userList : []
                }
            })
        }else{
            let value = []
            newValue.map((a)=>value.push({value : a.value,label:a.label}))
            this.setState({
                data:{
                    ...this.state.data,
                    userList : value
                }
            })
        }
    }

    getUser  = async () => {
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}user?size=30`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(async response=>{
            var user = []
            await Promise.all(response.list.map((e)=>user.push({value:e.userUniqueId,label:e.userCred.username})))
            this.setState({
                dataUsers : user
            })
        }).catch(err=>{
            console.log(err)
        });
    }

    handleSave = async () =>{
        this.setState({loading:true})
        const method = this.state.data.id ? 'PUT':'POST'
        let data = this.state.data
        let users = []
        Promise.all(this.state.data.userList.map((e)=>users.push({userUniqueId:e.value,uid:e.label})))
        data.userList = users
        axios({
            method : method,
            url : `${process.env.REACT_APP_API_URL}group`,
            headers: {'SSO-TOKEN' : localStorage.getItem('token')},
            data : {
                ...data
            }
        })
        .then((response)=> {
            this.getData()
            this.setState({modalIsOpen:false,loading:false})
        })
        .catch((error)=>{
            console.log(error.response)
        }).then(()=>{
            this.setState({loading:false})
        });
    }

    handlePageChange = (selected) => {
        this.setState({ 
            paginate : {
                ...this.state.paginate,
                page: selected
            }
         }, () => {
          this.getData();
        });
      };

    render(){
        return (
            <div className="px-4">
                <Loading visible={this.state.loading}/>
                <div className="flex items-center justify-between space-x-4 flex-1">
                    <h1 className="text-black text-2xl font-medium">Master Group</h1>
                    <button onClick={()=>{this.handleAdd()}} className="py-4 bg-mainblue focus:outline-none rounded px-6 flex items-center space-x-2 text-white">
                        <span>Add Group</span>
                    </button>
                </div>
                <div className="my-4 flex items-center justify-between">
                    <div className="flex items-center py-4 space-x-4 text-mainblue">
                        <FiUsers className="text-xl cursor-pointer text-mainblue"/>
                        <p>{this.state.paginate.total} Active Groups</p>
                    </div>
                    <div className="w-20p">
                        <Input name="search" onChange={(e)=>{this.handleFilter(e.target.value)}} icon={<FiSearch/>} placeholder="Search" />
                    </div>
                </div>
                <div className="w-full">
                    <table className="w-full bg-white rounded-lg">
                        <thead className="border-b text-left text-black">
                            <tr>
                                <th className="py-4 px-4">No</th>
                                <th>Group Code</th>
                                <th>Group Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.AllGroups.length === 0 ? (
                                    <tr>
                                        <td colSpan="4" className="py-8 px-4">
                                            <div className="flex items-center space-x-4 justify-center w-full text-center text-gray">
                                                <FiCloudOff/>
                                                <p>No data</p>
                                            </div>
                                        </td>
                                    </tr>
                                ) :(null)
                            }
                            {
                                this.state.AllGroups.map((group,key)=>{
                                    return(
                                        <tr key={key} className="hover:bg-table odd:bg-table">
                                            <td className="py-3 px-4">{key+1}</td>
                                            <td>{group.groupCode}</td>
                                            <td>{group.groupName}</td>
                                            <td>
                                                <div className="flex items-center space-x-2">
                                                    <FiEdit onClick={()=>{this.handleEdit(group.id)}} className="cursor-pointer"/>
                                                    <FiTrash onClick={()=>{this.setState({alert:true,data:{id:group.id}})}} className="cursor-pointer"/>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            
                        </tbody>
                    </table>
                </div>
                <Pagination 
                    onPageChange={(selected)=>{this.handlePageChange(selected)}} 
                    lastPage={this.state.paginate.lastPage} 
                    page={this.state.paginate.page} 
                    pageCount={this.state.paginate.pageCount} 
                />
                <MyModal md modalIsOpen={this.state.alert} setModal={()=>{this.setState({alert:false})}}>
                    <div className="flex justify-center flex-col items-center font-Poppins">
                        <h1 className="text-xl font-bold">Are you sure ?</h1>
                        <p>Do you want to delete this group?</p>
                        <div className="flex items-center space-x-2 mt-8">
                            <button onClick={()=>{this.handleDelete(this.state.data.id)}} className="p-4 focus:outline-none w-24 bg-red text-white rounded" >Yes</button>
                            <button onClick={()=>{this.setState({alert:false})}} className="p-4 focus:outline-none w-24 border text-black rounded" >Cancel</button>
                        </div>
                    </div>
                </MyModal>
                <MyModal modalIsOpen={this.state.modalIsOpen} setModal={()=>{this.setState({modalIsOpen:false})}}>
                    <div className="flex justify-between items-center font-Poppins">
                        <h1 className="text-xl font-bold">Create Group</h1>
                    </div>
                    <form  onSubmit={(e)=>{e.preventDefault();this.handleSave()}} className="mt-4 font-Poppins flex flex-col space-y-3">
                        <Input label="Group Code" name="groupCode" onChange={(e)=>{this.handleChange(e)}} value={this.state.data.groupCode} placeholder="Group Code" />
                        <Input label="Group Name" name="groupName" onChange={(e)=>{this.handleChange(e)}} value={this.state.data.groupName} placeholder="Group Name" />
                        <MySelect isMulti value={this.state.data.userList} onChange={(a,e)=>{this.handleSelectedChange(a,e)}} data={this.state.dataUsers} label="Users" placeholder="Select Users"/>
                        <div className="col-span-2">
                            <Button className="bg-mainblue float-right">{this.state.data.id ? 'Update' : 'Save'}</Button>
                        </div>
                    </form>
                </MyModal>
            </div>
        )
    }
}
