import React, { useState } from 'react'
// import { AiOutlineSearch } from 'react-icons/ai'

export default function Input({className,readOnly,label,icon,...props}) {
    const [focus, setFocus] = useState(false)
    return (
        <div className="w-full">
            <span className={`text-sm text-black font-medium`}>{label}</span>
            <div className={`flex items-center space-x-2 w-full p-4 rounded-md font-semibold text-black border-2 ${readOnly && 'cursor-not-allowed'} ${focus ?'bg-white border-button' :' border-transparent bg-input'} ${props.readOnly ? 'cursor-not-allowed' :''} ${className}`}>
                <input {...props} readOnly={readOnly} onBlur={()=>{setFocus(false)}} onFocus={()=>{setFocus(readOnly ? false : true)}} type="text" className={`placeholder-main w-full h-full ${readOnly && 'cursor-not-allowed'} bg-transparent focus:outline-none`}/>
                <div className="text-lg">
                    {icon}
                </div>
            </div>
        </div>
    )
}
