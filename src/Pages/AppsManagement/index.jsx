import React from 'react'
import CardApps from '../../Components/CardApps'
import Button from '../../Components/Button'
import Input from '../../Components/Input'
import MyModal from '../../Components/MyModal'
import { AiOutlinePicture } from 'react-icons/ai';
import { FiCamera, FiMinus, FiPlus } from 'react-icons/fi';
import Loading from '../../Components/Loading'
import AlertError from '../../Components/AlertError'

export default class Groups extends React.Component {

    state = {
        editMode : false,
        modalAddIsOpen : false,
        allApps : [],
        loading : false,
        alert : false,
        alertError : false,
        alertStatus : false,
        inputFields : {
            appId : '',
            linkApp : '',
            apiKey : '',
            title : '',
            desc : '',
            detail : [
                {
                    roleId  : '',
                    roleName: '',
                    roleCode: '',
                }
            ],
            add : [],
            update : [],
            delete : []
        }
    }
    handleDetailInputChange = (index, event, roleId) => {
        const values = [...this.state.inputFields.detail];
        if (event.target.name === "roleName") {
            values[index].roleName = event.target.value;
        } else if (event.target.name === "roleCode") {
            values[index].roleCode = event.target.value;
        }
        // const addValues = [...this.state.inputFields.add];
        // values.push({ roleName: '', roleCode: '' });
        // if (this.state.editMode) {
        //     addValues.push({ roleName: '', roleCode: '' });
        // }

        this.setState({
            inputFields : {
                ...this.state.inputFields,
                detail : values
            }
        })
    };
    componentDidMount(){
        this.getData()
    }
    getData = async () => {
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}app`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            this.setState({
                allApps : response.list,
                loading : false,
            })
        }).catch(err=>{
            this.setState({
                alertError : true,
                loading : false,
            })
        });
    }

    handleInputChange = (e) =>{
        this.setState({
            inputFields : {
                ...this.state.inputFields,
                [e.target.name] : e.target.value
            }
        })
    }

    handleAddFields = () => {
        const values = [...this.state.inputFields.detail];
        if (this.state.editMode) {
            values.push({ roleId : '',roleName: '', roleCode: '' });
        }else{
            values.push({roleName: '', roleCode: '' });
        }
        
        this.setState({
            inputFields : {
                ...this.state.inputFields,
                detail : values
            }
        })
      };
    
    handleSubmit = async () => {
        this.setState({loading:true})

        var Updated = []
        this.state.inputFields.detail.filter(function(value) {
            return value.roleId !== '';
        }).map((e)=>{
            return Updated.push({id:e.roleId,roleCode : e.roleCode,roleName:e.roleName})
        });

        var add = []
        this.state.inputFields.detail.filter(function(value) {
            return value.roleId === '' && (value.roleName !== '' && value.roleCode !== '');
        }).map((e)=>{
            return add.push({roleCode : e.roleCode,roleName:e.roleName})
        })
        
        this.setState({
            inputFields : {
                ...this.state.inputFields,
                add : add,
                update : Updated
            }
        },async ()=>{
            let url = `${process.env.REACT_APP_API_URL}app`
            let method = 'POST'
            let body = {}
            if (this.state.editMode) {
                url += `/${this.state.inputFields.appId}/multi`
                method = 'PUT'
                body = {
                    subroleAddList : this.state.inputFields.add,
                    subroleEditList : this.state.inputFields.update,
                    subroleDeleteList : this.state.inputFields.delete,
                } 
            }else{
                body = {subroleAddList : this.state.inputFields.detail}
            }
            
            return await fetch(url, {
                method: method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'SSO-TOKEN' : localStorage.getItem('token')
                },
                body: JSON.stringify({
                    appName    : this.state.inputFields.title,
                    domain     : this.state.inputFields.linkApp,
                    description: this.state.inputFields.desc,
                    apiKey     : this.state.inputFields.apiKey,
                    pict       : "test",
                    status : 1,
                    ...body
                })
            }).then((e)=>{
                // console.log(e)
                this.getData()
                this.setState({modalAddIsOpen:false,alertError : false,loading:false})
            }).catch(err=>{
                this.setState({ 
                    alertError : true,
                    loading : false,
                })
            });
        })
    }

    handleRemoveFields = (index,roleId) => {
        const values = [...this.state.inputFields.detail];
        values.splice(index, 1);
        if (roleId) {
            let deleteValues = [...this.state.inputFields.delete];
            deleteValues.push({id:roleId})
            // console.log(deleteValues)
            this.setState({
                inputFields : {
                    ...this.state.inputFields,
                    detail : values,
                    delete : deleteValues
                }
            })

        }else{
            this.setState({
                inputFields : {
                    ...this.state.inputFields,
                    detail : values
                }
            })
        }
    };

    handleUpdate = async id => {
        this.setState({loading : true})
        const AppHeader =  await fetch(`${process.env.REACT_APP_API_URL}app/${id}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        });
        const AppDetail =  await fetch(`${process.env.REACT_APP_API_URL}app/${id}/app-role`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        });

        const responHeader = await AppHeader.json()
        const responDetail = await AppDetail.json()
        let detail = []
        responDetail.list.map((value) => {
            return detail.push({
                roleId  : value.id,
                roleName: value.roleName,
                roleCode: value.roleCode,
            }) 
          });
        this.setState({
            editMode:true,
            modalAddIsOpen : true,
            loading : false,
            inputFields : {
                appId: id,
                linkApp: responHeader.domain,
                apiKey : responHeader.apiKey,
                title  : responHeader.appName,
                desc   : responHeader.description,
                detail : detail,
                delete : [],
                add : [],
                update : []
            }
        })
    }

    handleDelete = async (id) =>{
        this.setState({loading:true,inputField:{...this.state.inputFields,appId:id}})
        return await fetch(`${process.env.REACT_APP_API_URL}app/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(()=>{
            this.getData()
            this.setState({
                loading:false,
                alert:false
            })
        }).catch(err=>{
            console.log(err)
        });
    }

    handleActiveToggle =  async (id) => {
        this.setState({loading : true})
        return await fetch(`${process.env.REACT_APP_API_URL}app/${id}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            fetch(`${process.env.REACT_APP_API_URL}app/${id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'SSO-TOKEN' : localStorage.getItem('token')
                },
                body: JSON.stringify({
                    appName    : response.appName,
                    domain     : response.domain,
                    description: response.description,
                    apiKey     : response.apiKey,
                    status     : response.status === 1 ? 2 : 1
                })
            }).then((e)=>{
                // console.log(e)
                this.setState({alertStatus:false},()=>{
                    this.getData()
                })
            }).catch(err=>{
                console.log(err)
            });
        }).catch(err=>{
            console.log(err)
        });
    }

    handleAdd = () =>{
        this.setState({
            editMode : false,
            modalAddIsOpen : !this.state.modalAddIsOpen,
            inputFields : {
                linkApp : '',
                apiKey : '',
                title : '',
                desc : '',
                detail : [
                    {
                        roleName : '',
                        roleCode : '',
                    }
                ],
                delete : [],
                add : [],
                update : []
                
            }
        })
    }

    render(){
        return (
            <div className="px-4">
                  <Loading visible={this.state.loading}/>
                <div className="">
                    <div className="flex items-center justify-between">
                        <h1 className="font-medium text-xl">All Apps</h1>
                        <Button onClick={()=>{this.handleAdd()}} className="bg-mainblue ml-auto">Add New Integration</Button>
                    </div>
                    {this.state.allApps.length === 0 &&(
                        <div className="flex flex-col items-center justify-center w-full py-12 bg-white mt-6">
                            <img src={require('../../assets/img/empty.jpg')} alt="empty" className="w-30p"/>
                            <div className="flex flex-col items-center justify-center mt-6">
                                <h1 className="text-2xl text-gray">It's Empty Here</h1>
                                <p className="text-gray">It looks like you haven't integrated any apps yet</p>
                                <Button onClick={()=>{this.handleAdd()}} className="bg-mainblue mt-4">Add New Integration</Button>
                            </div>
                        </div>
                    )}
                    <div className="grid grid-cols-4 mt-4 gap-6">
                        {
                            this.state.allApps.map((app,i)=>(
                                <CardApps active={()=>{this.setState({alertStatus:true,inputField:{...this.state.inputFields,appId:app.id}})}} deletes={()=>{this.setState({alert:true,inputField:{...this.state.inputFields,appId:app.id}})}} update={()=>{this.handleUpdate(app.id)}} key={i} data={app}/>
                            ))
                        }
                    </div>
                </div>
                <MyModal lg modalIsOpen={this.state.modalAddIsOpen} setModal={()=>{this.setState({modalAddIsOpen:false})}}>
                    <div className="flex justify-center items-center font-Poppins">
                        <h1 className="text-xl font-bold text-center">New App Integration</h1>
                    </div>
                    <div className="flex mt-8">
                        <div className="rounded w-48 h-40 bg-gray overflow-hidden   flex items-center justify-center relative group">
                            <AiOutlinePicture className="text-6xl"/>
                            <div className="absolute px-2 flex items-center justify-center bg-gray h-full w-full inset-0 opacity-0 group-hover:opacity-100 transition duration-300 ease-in-out">
                                <button className="px-2 flex flex-col items-center justify-center focus:outline-none ">
                                    <FiCamera className="text-xl text-black"/>
                                    <span className="leading-none text-sm mt-2">Change Picture</span>
                                    <p className="text-xs">Max. File size : 2 Mb</p>
                                </button>
                            </div>
                        </div>
                        <div className=" w-full  ml-12">
                            <div className="grid grid-cols-2 gap-5">
                                <Input value={this.state.inputFields.linkApp} onChange={(e)=>{this.handleInputChange(e)}} name="linkApp" label="Link App" placeholder="Link App"/>
                                <Input value={this.state.inputFields.apiKey} onChange={(e)=>{this.handleInputChange(e)}} name="apiKey" label="API Key" placeholder="API Key"/>
                                <Input value={this.state.inputFields.title} onChange={(e)=>{this.handleInputChange(e)}} name="title" label="Title" placeholder="Title"/>
                                <Input value={this.state.inputFields.desc} onChange={(e)=>{this.handleInputChange(e)}} name="desc" label="Description" placeholder="Description"/>
                            </div>
                            <div className="mt-5 flex flex-col gap-5">
                                {
                                    this.state.inputFields.detail.map((inputField,index)=>(
                                        <div  key={`${inputField}~${index}`} className="flex items-center gap-5">
                                            {/* <div className="col-span-1">
                                                <Input value={inputField.roleId} readOnly name="roleId" placeholder="Role Id"/>
                                            </div> */}
                                            <div className="w-full">
                                                <Input label="Role Name" onChange={event => this.handleDetailInputChange(index, event, inputField.roleId)} value={inputField.roleName} name="roleName" placeholder="Role Name"/>
                                            </div>
                                            <div className="">
                                                <Input label="Role Code" onChange={event => this.handleDetailInputChange(index, event, inputField.roleId)} value={inputField.roleCode} name="roleCode" placeholder="Role Code"/>
                                            </div>
                                            <div className="flex items-center gap-3 mt-6">
                                                <div onClick={() => {this.state.inputFields.detail.length > 1 && this.handleRemoveFields(index,inputField.roleId)}} className={`flex items-center justify-center w-10 h-10 cursor-pointer hover:bg-opacity-75 rounded-full bg-red ${(this.state.inputFields.detail.length === 1 && index === 0) && 'bg-opacity-50 cursor-not-allowed'}`}>
                                                    <FiMinus/>
                                                </div>
                                                {this.state.inputFields.detail.length - 1 === index && (
                                                    <div onClick={() => this.handleAddFields()} className="flex items-center justify-center w-10 h-10 cursor-pointer hover:bg-opacity-75 rounded-full bg-green">
                                                        <FiPlus/>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                    </div>
                    <div className="flex justify-center mt-12">
                        <Button onClick={()=>this.handleSubmit()} className="bg-mainblue">Save</Button>
                    </div>
                    {/* <pre>
                    {JSON.stringify(this.state.inputFields, null, 2)}
                    </pre> */}
                </MyModal>
                <AlertError show={this.state.alertError} close={()=>{this.setState({alertError:false})}} OnRefresh={()=>{this.state.modalAddIsOpen ? this.handleSubmit() : this.getData()}} />
                <MyModal md modalIsOpen={this.state.alert} setModal={()=>{this.setState({alert:false})}}>
                    <div className="flex justify-center flex-col items-center font-Poppins">
                        <h1 className="text-xl font-bold">Are you sure ?</h1>
                        <p>Do you want to delete this App?</p>
                        <div className="flex items-center space-x-2 mt-8">
                            <button onClick={()=>{this.handleDelete(this.state.inputField.appId)}} className="p-4 focus:outline-none w-24 bg-red text-white rounded" >Yes</button>
                            <button onClick={()=>{this.setState({alert:false})}} className="p-4 focus:outline-none w-24 border text-black rounded" >Cancel</button>
                        </div>
                    </div>
                </MyModal>
                <MyModal md modalIsOpen={this.state.alertStatus} setModal={()=>{this.setState({alertStatus:false})}}>
                    <div className="flex justify-center flex-col items-center font-Poppins">
                        <h1 className="text-xl font-bold">Are you sure ?</h1>
                        <p>Do you want to Active this App?</p>
                        <div className="flex items-center space-x-2 mt-8">
                            <button onClick={()=>{this.handleActiveToggle(this.state.inputField.appId)}} className="p-4 focus:outline-none w-24 bg-red text-white rounded" >Yes</button>
                            <button onClick={()=>{this.setState({alertStatus:false})}} className="p-4 focus:outline-none w-24 border text-black rounded" >Cancel</button>
                        </div>
                    </div>
                </MyModal>
            </div>
        )

    }
}
