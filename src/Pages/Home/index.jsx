import React from 'react'

export default function Home() {
    return (
        <div>
            <div className="w-full rounded-lg bg-cover text-white p-10" style={{backgroundImage:`url(${require('../../assets/img/headerbg.png')})`}}>
                <h1 className="font-medium text-2xl">Selamat Datang</h1>
                <h1 className="font-medium text-4xl">Muhammad Nuh</h1>
            </div>
            <div className="mt-8">
                <h1 className="font-medium text-xl">All Apps</h1>
            </div>
        </div>
    )
}
