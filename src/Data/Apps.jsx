export const appsData = [
    {
        value : 'SPMB',
        label : 'SPMB',
    },
    {
        value : 'CBT Terintegrasi',
        label : 'CBT Terintegrasi',
    },
    {
        value : 'UKT',
        label : 'UKT',
    },
    {
        value : 'H2H',
        label : 'H2H',
    },
    {
        value : 'SIAK +Elearning',
        label : 'SIAK +Elearning',
    },
    {
        value : 'Gateway Feeder',
        label : 'Gateway Feeder',
    },
    {
        value : 'E-Letter',
        label : 'E-Letter',
    },
    {
        value : 'Persuratan Online',
        label : 'Persuratan Online',
    },
    {
        value : 'Beasiswa',
        label : 'Beasiswa',
    },
    {
        value : 'Pegawai',
        label : 'Pegawai',
    },
    {
        value : 'Notifikasi',
        label : 'Notifikasi',
    },
    {
        value : 'API',
        label : 'API',
    },
    {
        value : 'LKP',
        label : 'LKP',
    },
    {
        value : 'BKD Remunerasi',
        label : 'BKD Remunerasi',
    },
]