import React from 'react'
import { FiUsers,FiPlus } from "react-icons/fi";
import {usersData} from "../../Data/Users"
import { FaTrash } from "react-icons/fa";
import { MdModeEdit } from "react-icons/md";
import Input from "../../Components/Input"
import MyModal from "../../Components/MyModal"
import Button from '../../Components/Button';

export default function Users() {
    const [modalIsOpen,setIsOpen] = React.useState(false);
    return (
        <div className="p-10">
            <div className="flex justify-between items-center">
                <div>
                    <h1 className="font-bold text-2xl">Users</h1>
                    <div className="flex items-center space-x-8 mt-4">
                        <div className="flex items-center">
                            <div className="bg-mainblue bg-opacity-25 rounded-full w-10 h-10 flex justify-center items-center">
                                <FiUsers className="text-mainblue font-bold"/>
                            </div>
                            <h1 className="font-semibold ml-3">8 Active Users</h1>
                        </div>
                    </div>
                </div>
                <div onClick={()=>{setIsOpen(true)}} className="flex justify-center items-center w-12 h-12 bg-mainblue rounded-full shadow-blue cursor-pointer hover:bg-opacity-75 group">
                    <FiPlus className="text-white text-3xl group-hover:text-mainblue"/>
                </div>
            </div>
            <div className="mt-8 overflow-x-auto min-w-full">
                <table style={{borderCollapse:"separate",borderSpacing:"0 .75rem"}} className="min-w-full">
                    <thead>
                        <tr className="text-left bg-input text-mainblue rounded-md">
                            <th className="px-4 whitespace-no-wrap py-4 rounded-l-md">No</th>
                            <th className="-lg:px-4 whitespace-no-wrap">User Id</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Username</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Full Name</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Email</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Group</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Role</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Status</th>
                            <th className="-lg:px-4 whitespace-no-wrap rounded-r-md text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            usersData.map((data,key)=>{
                                return(
                                    <tr key={key} className="text-left bg-white text-black rounded-md font-semibold hover:bg-mainblue hover:bg-opacity-25 hover:text-mainblue">
                                        <td className="px-4 py-4 rounded-l-md text-center">{data.id}</td>
                                        <td className="-lg:px-4">{data.userid}</td>
                                        <td className="-lg:px-4">{data.username}</td>
                                        <td className="-lg:px-4">{data.fullname}</td>
                                        <td className="-lg:px-4">{data.email}</td>
                                        <td className="-lg:px-4">{data.group}</td>
                                        <td className="-lg:px-4">{data.role}</td>
                                        {(()=>{
                                            if (data.status === "Active") {
                                                return(
                                                    <td className="-lg:px-4 text-green">{data.status}</td>
                                                )
                                            }else if (data.status === "InActive") {
                                                return(
                                                    <td className="-lg:px-4 text-red">{data.status}</td>
                                                )
                                            }
                                        })()}
                                        <td className="-lg:px-4 rounded-r-md text-center">
                                            <div className="flex space-x-2 items-center justify-center">
                                                <MdModeEdit size={20} className="cursor-pointer text-black"/>
                                                <FaTrash size={20} className="text-red cursor-pointer"/>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                
            </div>
            <MyModal modalIsOpen={modalIsOpen} setModal={()=>{setIsOpen(false)}}>
                <div className="flex justify-between items-center font-Poppins">
                    <h1 className="text-xl font-bold">Create Users</h1>
                </div>
                <div className="grid grid-cols-2 gap-5 mt-4 font-Poppins">
                    <div className="col-span-2">
                        <Input placeholder="Username" />
                    </div>
                    <Input placeholder="Fullname" />
                    <Input placeholder="Email" />
                    <select className="form-select p-4 font-semibold rounded-md placeholder-main text-gray bg-input focus:outline-none w-full">
                        <option value={0} className="text-gray">Group</option>
                        <option value="Foo" className="text-gray">Foo</option>
                        <option value="Bar" className="text-gray">Bar</option>
                        
                    </select>
                    <select className="form-select p-4 font-semibold rounded-md placeholder-main text-gray bg-input focus:outline-none w-full">
                        <option value={0} className="text-gray">Role</option>
                        <option value="Foo" className="text-gray">Foo</option>
                        <option value="Bar" className="text-gray">Bar</option>
                    </select>
                    <div onClick={()=>{setIsOpen(false)}} className="col-span-2">
                        <Button className="bg-mainblue float-right">Save</Button>
                    </div>
                </div>
            </MyModal>
        </div>
    )
}
