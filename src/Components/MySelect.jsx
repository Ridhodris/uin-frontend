import React from 'react'
import Select from 'react-select'

export default function MySelect({label,placeholder,isMulti,data,...props}) {
    return (
        <div>
            <span className={`text-sm text-black font-medium`}>{label}</span>
            <Select
            {...props}
            isClearable
            placeholder={placeholder}
            closeMenuOnSelect={!isMulti}
            isMulti={isMulti}
            menuPortalTarget={document.body}  
            options={data}
            // getOptionLabel={(option)=>option.[optionLabel]}
            // getOptionValue={(option)=>option.id} 
            styles={{ 
                control:(provided, state)=>({
                    // ...provided,
                    display:'flex',
                    padding: '0.50rem',
                    backgroundColor: state.isFocused ? 'white': '#EBEBF2',
                    borderRadius:'0.375rem',
                    borderWidth:'2px',
                    borderColor: !state.isFocused ? 'transparent' : '#A4A4BA'
                }),
                multiValue:(provided)=>({
                    ...provided,
                    backgroundColor : 'hsl(192deg 62% 85%)'
                }),
                option:(provided,state)=>({
                    ...provided,
                    backgroundColor : state.isSelected ? '#029CC3' : state.isFocused ?'hsl(192deg 62% 85%)' :null ,
                    // color : '#343544'
                }),
                menuPortal: base => ({ ...base, zIndex: 45 })
                }}
        />
        </div>
    )
}
