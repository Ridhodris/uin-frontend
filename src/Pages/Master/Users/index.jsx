import React from 'react'
import { FiEdit, FiLock, FiSearch, FiTrash, FiUsers } from 'react-icons/fi';
import Button from '../../../Components/Button';
import Input from '../../../Components/Input';
import Loading from '../../../Components/Loading';
import MyModal from "../../../Components/MyModal"
import MySelect from '../../../Components/MySelect'
const axios = require('axios');

const TableUsers = ()=>{
    return(
        <table className="w-full bg-white rounded-lg">
            <thead className="border-b text-left text-black">
                <tr>
                    <th className="py-4 px-4">No</th>
                    <th>User ID</th>
                    <th>Username</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Group</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                {(()=>{
                        const element = [];
                        for (let index = 0; index < 10; index++) {
                            element.push(
                                <tr key={index} className="hover:bg-table odd:bg-table">
                                    <td className="py-3 px-4">{index +1}</td>
                                    <td>UIN1234</td>
                                    <td>ahmadsyahputra</td>
                                    <td>Ahmad Syahputra</td>
                                    <td>ahmadsyah@mail.com</td>
                                    <td>Hukum</td>
                                    <td>Dosen</td>
                                    <td className="text-green">Active</td>
                                    <td>
                                        <div className="flex items-center space-x-2">
                                            <FiEdit onClick={()=>{this.setState({edit:true,modalIsOpen:true})}} className="cursor-pointer"/>
                                            <FiLock onClick={()=>{this.setState({alertStatus:true})}} className="cursor-pointer"/>
                                            <FiTrash onClick={()=>{this.setState({alertDelete:true})}} className="cursor-pointer"/>
                                        </div>
                                    </td>
                                </tr>
                            )
                        }
                        return element
                })()}
                
            </tbody>
        </table>
    )
}

export default class Users extends React.Component {
    state = {
        loading         : false,
        modalIsOpen     : false,
        modalRoleMapping: false,
        alertDelete     : false,
        alertStatus     : false,
        edit            : false,
        allChecked      : false,
        setRole         : 0,
        filterRole      : 0,
        filterGroup     : 0,
        dataRole        : [],
        dataGroup       : [],
        dataUsers       : []
    }

    componentDidMount(){
        Promise.all([this.getRole(),this.getGroup()])
    }

    getRole = async () => {
        return await fetch(`${process.env.REACT_APP_API_URL}master-role`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{

            let roleTemp = []
            response.list.map((e)=>roleTemp.push({label:e.roleName,value:e.id}))

            this.setState({
                dataRole : roleTemp,
                loading : false,
            })
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                loading : false,
            })
        });
    }

    getGroup = async () => {
        return await fetch(`${process.env.REACT_APP_API_URL}group`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            let groupTemp = []
            response.list.map((e)=>groupTemp.push({label:e.groupName,value:e.id}))
            this.setState({
                dataGroup : groupTemp,
                loading : false,
            })
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                loading : false,
            })
        });
    }

    getUserMapping =  async () => {
        this.setState({loading:true})
        await axios.get(`${process.env.REACT_APP_API_URL}role/map?size=1000000`,{
            headers: {
                'SSO-TOKEN' : localStorage.getItem('token')
            }
        })
        .then((response)=> {
            var data = []
            response.data.list.map((e)=> {return data.push({id:e.id,userUniqueId:e.userUniqueId,isChecked:false})})
            this.setState({dataUsers:data,allChecked:false})
        })
        .catch((error)=>{
            console.log(error.response.data)
        }).then(()=>{
            this.setState({loading:false})
        });
    }

    handleCheked = (e) => {
        let itemName = e.target.name;
        let checked = e.target.checked;
        this.setState(prevState => {
        let { dataUsers, allChecked } = prevState;
        if (itemName === "checkAll") {
            allChecked = checked;
            dataUsers = dataUsers.map(item => ({ ...item, isChecked: checked }));
        } else {
            dataUsers = dataUsers.map(item =>
            item.userUniqueId === itemName ? { ...item, isChecked: checked } : item
            );
            allChecked = dataUsers.every(item => item.isChecked);
        }
        return { dataUsers, allChecked };
        });
    }

    handleUnCheked = (e) => {
        this.setState(prevState => {
            let { dataUsers } = prevState;
            dataUsers = dataUsers.map(item =>
            item.userUniqueId === e ? { ...item, isChecked: false } : item
            );
            return { dataUsers};
            });
    }

    handleRoleMappingSave = async () => {
        let data = []
        this.state.dataUsers.filter(x => x.isChecked === true).map((e)=>{
            return data.push({userUniqueId:e.userUniqueId})
        })
        let datas = {
            roleId : this.state.setRole,
            userList : data
        }
        this.setState({loading:true})
        await axios.put(`${process.env.REACT_APP_API_URL}role/map`,{...datas},
        {
            headers: {
                'SSO-TOKEN' : localStorage.getItem('token')
            }
        })
        .then((response)=> {
           
            this.getUserMapping()
        })
        .catch((error)=>{
            console.log(error.response.data)
        }).then(()=>{
            this.setState({loading:false})
        });
    }

    handleSelectChange = (newValue, selectedOptions) => {
        if (selectedOptions.action === "clear") {
            this.setState({
                [selectedOptions.name] : 0
            },()=>{
                if (this.state.filterRole === 0 && this.state.filterGroup === 0) {
                    this.getUserMapping()
                }
            })
        }else{
            this.setState({
                [selectedOptions.name] : newValue.value
            },async ()=>{
                if (this.state.filterRole !== 0 && this.state.filterGroup !== 0) {
                    this.setState({loading:true})
                    await axios.get(`${process.env.REACT_APP_API_URL}role/${this.state.filterGroup}/${this.state.filterRole}/map`,{
                        headers: {
                            'SSO-TOKEN' : localStorage.getItem('token')
                        }
                    })
                    .then((response)=> {
                        if (response.data.length > 0) {
                            var data = []
                            response.data.list.map((e)=>data.push({id:e.id,userUniqueId:e.userUniqueId,isChecked:false}))
                            this.setState({dataUsers:data})
                        }else{
                            this.setState({dataUsers:[]})
                        }
                    })
                    .catch((error)=>{
                        console.log(error)
                    }).then(()=>{
                        this.setState({loading:false})
                    });
                }
            })
        }
    }

    render(){
        return (
            <div className="px-4">
                <Loading visible={this.state.loading}/>
                <div className="flex items-center justify-between space-x-4 flex-1">
                    <h1 className="text-black text-2xl font-medium">Users</h1>
                    <div className="flex items-center space-x-5">
                        <Button onClick={()=>{this.getUserMapping();this.setState({modalRoleMapping:true})}} className="bg-mainblue">
                            <span>Role Mapping</span>
                        </Button>
                        <Button onClick={()=>{this.setState({modalIsOpen:true,edit:false})}} className="bg-mainblue">
                            <span>Add Users</span>
                        </Button>
                    </div>
                </div>
                <div className="my-4 flex items-center justify-between">
                    <div className="flex items-center py-4 space-x-4 text-mainblue">
                        <FiUsers className="text-xl cursor-pointer text-mainblue"/>
                        <span>200 Active Users</span>
                    </div>
                    <div className="w-20p">
                        <Input name="search" onChange={(e)=>{this.handleFilter(e.target.value)}} icon={<FiSearch/>} placeholder="Search" />
                    </div>
                </div>
                <div className="w-full">
                    <TableUsers/>
                </div>
                <MyModal lg modalIsOpen={this.state.modalIsOpen} setModal={()=>{this.setState({modalIsOpen:false})}}>
                    <div className="flex justify-center items-center font-Poppins">
                        <h1 className="text-xl font-bold text-center">{this.state.edit ? 'User Detail' :'Add New User'}</h1>
                    </div>
                    <div className="grid grid-cols-3 gap-5 mt-8">
                        <Input label="Username" name="username" placeholder="Username"/>
                        <Input label="Full Name" name="fullname" placeholder="Full Name"/>
                        <Input label="Email" name="email" placeholder="Email"/>
                        <div className="col-span-3 grid grid-cols-2 gap-5">
                            <MySelect data={this.state.dataRole} label="Role" placeholder="Role"/>
                            <MySelect data={this.state.dataGroup} label="Group" placeholder="Group"/>
                        </div>
                    </div>
                    <div className="flex justify-center mt-8">
                        <Button className="bg-mainblue">Save</Button>
                    </div>
                </MyModal>
                <MyModal modalIsOpen={this.state.modalRoleMapping} setModal={()=>{this.setState({modalRoleMapping:false})}}>
                    <div className="flex justify-center items-center font-Poppins">
                        <h1 className="text-xl font-bold text-center">Role Mapping</h1>
                    </div>
                    <div className="mt-8 flex gap-5">
                        <div className="w-full">
                            <div className="grid grid-cols-2 gap-5 mt-8">
                                <MySelect data={this.state.dataGroup} name="filterGroup"  onChange={(a,b)=>this.handleSelectChange(a,b)}  label="Filter Group" placeholder="Select Group"/>
                                <MySelect data={this.state.dataRole} name="filterRole"  onChange={(a,b)=>this.handleSelectChange(a,b)}  label="Filter Role" placeholder="Select Role"/>
                            </div>
                            <div className="h-50vh overflow-y-auto mt-6 border-t">
                                <table className="w-full bg-white rounded-lg">
                                    <thead className="border-b text-left text-black">
                                        <tr>
                                            <th className="py-4 px-4 w-10">
                                            <input  checked={this.state.allChecked} name="checkAll"  onChange={(e)=>this.handleCheked(e)} type="checkbox" className="form-checkbox text-mainblue text-xl"/>
                                            </th>
                                            <th>No</th>
                                            <th>User ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataUsers.map((e,i)=>{
                                            return(
                                                <tr key={i} className="hover:bg-table odd:bg-table">
                                                    <td className="py-3 px-4">
                                                        <input type="checkbox" name={e.userUniqueId} onChange={(e)=>this.handleCheked(e)} checked={e.isChecked} value={e.userUniqueId} className="form-checkbox text-mainblue text-xl"/>
                                                    </td>
                                                    <td> {i+1} </td>
                                                    <td> {e.userUniqueId} </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="w-full">
                            <div className="mt-8">
                                <MySelect data={this.state.dataRole} label="Set Role" name="setRole" onChange={(a,b)=>this.handleSelectChange(a,b)} placeholder="Select Role"/>
                            </div>
                            <div className="h-50vh overflow-y-auto mt-6 border-t">
                                <table className="w-full bg-white rounded-lg">
                                    <thead className="border-b text-left text-black">
                                        <tr>
                                            <th className="py-4 px-4">No</th>
                                            <th>User ID</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataUsers.filter(x => x.isChecked === true).map((e,i)=>{
                                            return(
                                                <tr key={i} className="hover:bg-table odd:bg-table">
                                                    <td className="py-3 px-4">{i+1}</td>
                                                    <td> {e.userUniqueId} </td>
                                                    <td>
                                                        <div className="flex justify-center items-center space-x-2">
                                                            <FiTrash onClick={()=>this.handleUnCheked(e.userUniqueId)} className="cursor-pointer"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <Button onClick={()=>this.handleRoleMappingSave()} className="bg-mainblue mt-5 float-right">Save</Button>
                </MyModal>
                <MyModal md modalIsOpen={this.state.alertDelete} setModal={()=>{this.setState({alertDelete:false})}}>
                    <div className="flex justify-center flex-col items-center font-Poppins">
                        <h1 className="text-xl font-bold">Are you sure ?</h1>
                        <p>Do you want to delete this User?</p>
                        <div className="flex items-center space-x-2 mt-8">
                            <button onClick={()=>{}} className="p-4 focus:outline-none w-24 bg-red text-white rounded" >Yes</button>
                            <button onClick={()=>{this.setState({alertDelete:false})}} className="p-4 focus:outline-none w-24 border text-black rounded" >Cancel</button>
                        </div>
                    </div>
                </MyModal>
                <MyModal md modalIsOpen={this.state.alertStatus} setModal={()=>{this.setState({alertStatus:false})}}>
                    <div className="flex justify-center flex-col items-center font-Poppins">
                        <h1 className="text-xl font-bold">Are you sure ?</h1>
                        <p>Do you want to Active this User?</p>
                        <div className="flex items-center space-x-2 mt-8">
                            <button onClick={()=>{}} className="p-4 focus:outline-none w-24 bg-red text-white rounded" >Yes</button>
                            <button onClick={()=>{this.setState({alertStatus:false})}} className="p-4 focus:outline-none w-24 border text-black rounded" >Cancel</button>
                        </div>
                    </div>
                </MyModal>
            </div>
        )

    }
}
