import React, { useState } from 'react'
import { Link, NavLink  } from 'react-router-dom'
import { FiBell, FiFilter, FiGrid, FiHome, FiLock, FiMaximize, FiMenu, FiPower, FiSettings, FiUser, FiUsers } from "react-icons/fi";
import { RiUserSettingsLine } from "react-icons/ri";
import { Route, Switch,useRouteMatch } from 'react-router-dom'
import Loader from 'react-loader-spinner'

const Groups = React.lazy(()=>import('./Master/Groups'));
const Roles = React.lazy(()=>import('./Master/Roles'));
// const Users = React.lazy(()=>import('./Master/Users'));
const Home = React.lazy(()=>import('./Home'));
const Profile = React.lazy(()=>import('./Profile'));
const Report = React.lazy(()=>import('./Report'));
const Setting = React.lazy(()=>import('./Setting'));
const Lecturer = React.lazy(()=>import('./UserManagement/Lecturer'));
const Student = React.lazy(()=>import('./UserManagement/Student'));
const Admin = React.lazy(()=>import('./UserManagement/Admin'));
const Appsmanagement = React.lazy(()=>import('./AppsManagement'));


const SideBar = () =>{
    return(
        <div className="bg-white h-full w-64 flex flex-col justify-between">
            <div>
                <div className="pl-6 pt-6">
                    <img src={require('../assets/img/logo.png')} alt="logo" className="w-16"/>
                    <h1 className="mt-4 leading-none font-medium">UIN Syarif Hidayatullah</h1>
                    <p className="text-sm font-medium">Jakarta</p>
                </div>
                <div className="mt-8 flex flex-col border-b">
                    <div className="py-3">
                        <NavLink  to="/fe/home" className="pl-4 cursor-pointer flex items-center space-x-4 hover:text-mainblue border-l-4 border-transparent text-black">
                            <FiHome/>
                            <span>Home</span>
                        </NavLink>
                    </div>
                    <div className="py-3">
                        <NavLink  to="/fe/profile" className="pl-4 cursor-pointer flex items-center space-x-4 hover:text-mainblue border-l-4 border-transparent text-black">
                            <FiUser/>
                            <span>Profile</span>
                        </NavLink>
                    </div>
                </div>
                <div className="flex flex-col">
                    
                    <div className="py-3">
                        <div className="pl-4 cursor-pointer flex items-center space-x-4 hover:text-mainblue border-l-4 border-transparent text-black">
                            <FiUsers/>
                            <span>User Management</span>
                        </div>
                        <div className="flex flex-col space-y-3 mt-3">
                            <NavLink to="/fe/lecturer" className="pl-12 cursor-pointer hover:text-mainblue border-l-4 border-transparent text-black">
                                <span>Lecturer</span>
                            </NavLink>
                            <NavLink to="/fe/student" className="pl-12 cursor-pointer hover:text-mainblue border-l-4 border-transparent text-black">
                                <span>Student</span>
                            </NavLink>
                            <NavLink to="/fe/admin" className="pl-12 cursor-pointer hover:text-mainblue border-l-4 border-transparent text-black">
                                <span>Admin</span>
                            </NavLink>
                        </div>
                    </div>
                    <div className="py-3">
                        <NavLink to="/fe/appsmanagement" className="pl-4 cursor-pointer flex items-center space-x-4 hover:text-mainblue border-l-4 border-transparent text-black">
                            <FiGrid/>
                            <span>Apps Management</span>
                        </NavLink>
                    </div>
                    <div className="py-3">
                        <div className="pl-4 cursor-pointer flex items-center space-x-4 hover:text-mainblue border-l-4 border-transparent text-black">
                            <RiUserSettingsLine/>
                            <span>Role Management</span>
                        </div>
                        <div className="flex flex-col space-y-3 mt-3">
                            <NavLink to="/fe/roles" className="pl-12 cursor-pointer hover:text-mainblue border-l-4 border-transparent text-black">
                                <span>Roles</span>
                            </NavLink>
                            {/* <NavLink to="/fe/users" className="pl-12 cursor-pointer hover:text-mainblue border-l-4 border-transparent text-black">
                                <span>Users</span>
                            </NavLink> */}
                            <NavLink to="/fe/groups" className="pl-12 cursor-pointer hover:text-mainblue border-l-4 border-transparent text-black">
                                <span>Groups</span>
                            </NavLink>
                        </div>
                    </div>

                    <div className="py-3">
                        <NavLink to="/fe/report" className="pl-4 cursor-pointer flex items-center space-x-4 hover:text-mainblue border-l-4 border-transparent text-black">
                            <FiFilter/>
                            <span>Report</span>
                        </NavLink>
                    </div>
                    
                    <div className="py-3">
                        <NavLink to="/fe/setting" className="pl-4 cursor-pointer flex items-center space-x-4 hover:text-mainblue border-l-4 border-transparent text-black">
                            <FiSettings/>
                            <span>Setting</span>
                        </NavLink>
                    </div>
                </div>
            </div>
            <p className="text-xs pb-8 pl-4">Version 1.0</p>
        </div>
    )
}

const TopBar = () => {
    const [show, setShow] = useState(false)
    const toggleFullScreen = () => {
        if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
         (!document.mozFullScreen && !document.webkitIsFullScreen)) {
          if (document.documentElement.requestFullScreen) {  
            document.documentElement.requestFullScreen();  
          } else if (document.documentElement.mozRequestFullScreen) {  
            document.documentElement.mozRequestFullScreen();  
          } else if (document.documentElement.webkitRequestFullScreen) {  
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
          }  
        } else {  
          if (document.cancelFullScreen) {  
            document.cancelFullScreen();  
          } else if (document.mozCancelFullScreen) {  
            document.mozCancelFullScreen();  
          } else if (document.webkitCancelFullScreen) {  
            document.webkitCancelFullScreen();  
          }  
        }  
      }
    
    return (
        <div className="bg-white py-4 flex items-center justify-between px-6">
            <div className="flex items-center space-x-4 text-xl">
                <FiMenu className="cursor-pointer"/>
                <FiMaximize onClick={toggleFullScreen} className="cursor-pointer"/>
            </div>
            <div className="flex items-center space-x-8">
                <div className="flex items-center space-x-4">
                    <FiBell className="cursor-pointer text-xl"/>
                </div>
                <div className="ml-4 relative">
                    <div  className="flex items-center space-x-4 ">
                        <p>M. Nuh</p>
                        <div onClick={()=>setShow(!show)} className="flex items-center justify-center bg-mainblue w-10 h-10 rounded-full cursor-pointer">
                            <h1 className="text-white text-lg select-none">M</h1>
                        </div>
                    </div>
                    <div   className={`w-56 ${show ? '' : 'hidden'} border rounded-lg overflow-hidden bg-white absolute top-0 mt-12 shadow-xl right-0`}>
                        <ul>
                            <Link to="/fe/setting" className="flex p-4 items-center space-x-4 cursor-pointer hover:bg-mainblue hover:bg-opacity-25">
                                <FiLock className="text-black"/>
                                <span className="select-none" >Change Password</span>
                            </Link>
                            <NavLink to="/login" className="flex p-4 items-center space-x-4 cursor-pointer hover:bg-mainblue hover:bg-opacity-25">
                                <FiPower className="text-black"/>
                                <span className="select-none" >Logout</span>
                            </NavLink>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default function Fe() {
    let { path } = useRouteMatch();
    return (
        <div style={{backgroundColor:'#F5F6FA'}} className="h-screen flex">
            <SideBar/>
            <div className="flex flex-col overflow-y-auto flex-1">
                <TopBar/>
                <div className="p-8 flex-1">
                <React.Suspense fallback={
                   <div className="h-screen absolute inset-0 flex justify-center items-center w-full">
                       <Loader
                            type="Puff"
                            color="#029cc3"
                            height={100}
                            width={100}
                            timeout={3000}
                        />
                   </div>
                }>
                    <Switch>
                        <Route path={`${path}/home`} component={Home}/>
                        <Route path={`${path}/profile`} component={Profile}/>
                        <Route path={`${path}/report`} component={Report}/>
                        <Route path={`${path}/lecturer`} component={Lecturer}/>
                        <Route path={`${path}/student`} component={Student}/>
                        <Route path={`${path}/admin`} component={Admin}/>
                        <Route path={`${path}/roles`} component={Roles}/>
                        <Route path={`${path}/groups`} component={Groups}/>
                        <Route path={`${path}/setting`} component={Setting}/>
                        {/* <Route path={`${path}/users`} component={Users}/> */}
                        <Route path={`${path}/appsmanagement`} component={Appsmanagement}/>
                    </Switch>
                </React.Suspense>
                </div>
            </div>
        </div>
    )
}
