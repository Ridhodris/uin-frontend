import React from 'react'

export default function Button({className,onClick,children}) {
    return (
        <button onClick={onClick} className={`hover:opacity-75 w-48 rounded-md py-4 text-white font-semibold focus:outline-none ${className}`}>{children}</button>

    )
}
