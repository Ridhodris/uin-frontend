import React from 'react'
import { AiOutlinePicture } from 'react-icons/ai'
import { FiCamera } from 'react-icons/fi'
import Button from '../../Components/Button'
import Input from '../../Components/Input'

export default function Setting() {
    return (
        <div className="px-4">
            <h1 className="text-black font-medium text-2xl">Setting</h1>
            <div className="flex mt-6 items-start space-x-8">
                <div className="rounded w-40 h-40 bg-white overflow-hidden   flex items-center justify-center relative group">
                    <AiOutlinePicture className="text-6xl"/>
                    <div className="absolute px-2 flex items-center justify-center bg-gray h-full w-full inset-0 opacity-0 group-hover:opacity-100 transition duration-300 ease-in-out">
                        <button className="px-2 flex flex-col items-center justify-center focus:outline-none ">
                            <FiCamera className="text-xl text-black"/>
                            <span className="leading-none text-sm mt-2">Change Picture</span>
                        </button>
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-4 flex-1">
                    <div className="grid grid-cols-1 gap-4">
                        <Input label="Nama Lengkap" name="full_name" placeholder="Nama Lengkap"/> 
                        <Input readOnly label="Alamat Email" name="email" placeholder="Alamat Email"/> 
                        <Input label="No. Telp" name="telp" placeholder="No. Telp"/> 
                    </div>
                    <div className="grid grid-cols-1 gap-4">
                        <Input label="Old Password" name="old_password" placeholder="Old Password"/> 
                        <Input label="New Password" name="new_password" placeholder="New Password"/> 
                        <Input label="Confirm Password" name="comfirm_password" placeholder="Confirm Password"/> 
                    </div>
                    <div>
                        <Button className="bg-mainblue">Save</Button>
                    </div>
                </div>
            </div>
        </div>
    )
}
