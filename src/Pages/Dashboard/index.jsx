import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'
import Loader from 'react-loader-spinner'

const Home = React.lazy(()=>import('./Home'));
const Siderbar = React.lazy(()=>import('./Siderbar'));
const Users = React.lazy(()=>import('./Users'));
const Settings = React.lazy(()=>import('./Settings'));
const Groups = React.lazy(()=>import('./Groups'));

export default function Dashboard() {
    let { path } = useRouteMatch();

    return (
        <div className="h-screen flex -lg:flex-col">
           <Siderbar/>
           <div className="flex-1 overflow-y-auto overflow-x-hidden bg-main">
               <React.Suspense fallback={
                   <div className="h-screen absolute inset-0 flex justify-center items-center w-full">
                       <Loader
                            type="Puff"
                            color="#029cc3"
                            height={100}
                            width={100}
                            timeout={3000}
                        />
                   </div>
               }>
                    <Switch>
                        <Route path={`${path}/home`} component={Home}/>
                        <Route path={`${path}/groups`} component={Groups}/>
                        <Route path={`${path}/users`} component={Users}/>
                        <Route path={`${path}/Settings`} component={Settings}/>
                    </Switch>
               </React.Suspense>
           </div>
        </div>
    )
}
