import React from 'react'
import { AiOutlinePicture } from "react-icons/ai";
import { FiCamera } from 'react-icons/fi';

const Input = ({placeholder}) => {
    return (
        <input type="text" placeholder={placeholder} className="py-4 px-4 bg-white rounded w-full focus:outline-none"/>
    )
}
const TextArea = ({placeholder}) => {
    return (
        <textarea placeholder={placeholder} className="py-4 px-2 bg-white rounded w-full focus:outline-none" ></textarea>

    )
}

export default function Profile() {
    return (
        <div>
            <div className="px-4 border-b-2 pb-6">
                <h1 className="text-black font-medium text-2xl">Data Pribadi</h1>
                <div className="flex mt-6 items-start">
                    <div className="rounded w-40 h-40 bg-white overflow-hidden   flex items-center justify-center relative group">
                        <AiOutlinePicture className="text-6xl"/>
                        <div className="absolute px-2 flex items-center justify-center bg-gray h-full w-full inset-0 opacity-0 group-hover:opacity-100 transition duration-300 ease-in-out">
                            <button className="px-2 flex flex-col items-center justify-center focus:outline-none ">
                                <FiCamera className="text-xl text-black"/>
                                <span className="leading-none text-sm mt-2">Change Picture</span>
                            </button>
                        </div>
                    </div>
                    <div className="grid grid-cols-3 gap-4 ml-8 text-black w-full">
                        <div className="col-span-2">
                            <span className="text-sm">Nama Lengkap</span>
                            <Input placeholder="Masukan Nama Lengkap" />
                        </div>
                        <div className="">
                            <span className="text-sm">Nomor Identitas (NIK)</span>
                            <Input placeholder="Masukan NIK" />
                        </div>
                        <div className="">
                            <span className="text-sm">Tempat Lahir</span>
                            <Input placeholder="Masukan Tempat Lahir" />
                        </div>
                        <div className="">
                            <span className="text-sm">Tanggal Lahir</span>
                            <Input placeholder="dd/mm/yyyy" />
                        </div>
                        <div className="">
                            <span className="text-sm">Jenis Kelamin</span>
                            <Input placeholder="Pilih" />
                        </div>
                        <div className="">
                            <span className="text-sm">Kewarganegaraan</span>
                            <Input placeholder="Pilih" />
                        </div>
                        <div className="">
                            <span className="text-sm">Agama</span>
                            <Input placeholder="Pilih" />
                        </div>
                        <div className="">
                            <span className="text-sm">Nama Ibu Kandung</span>
                            <Input placeholder="Masukan Nama Ibu Kandung" />
                        </div>
                        <div className="col-span-2">
                            <span className="text-sm">Alamat Email</span>
                            <Input placeholder="Masukan Alamat Email" />
                        </div>
                        <div className="">
                            <span className="text-sm">No. Telp</span>
                            <Input placeholder="Masukan No. Telp" />
                        </div>
                    </div>
                </div>
            </div>
            <div className="border-b-2 pb-6 mt-6">
                <h1 className="text-black font-medium text-2xl">Data Alamat Asal</h1>
                <div className="grid grid-cols-3 gap-4 mt-6 text-black">
                    <div className="col-span-2">
                        <span className="text-sm">Alamat</span>
                        <TextArea placeholder="Alamat Lengkap..." />
                    </div>
                    <div className="">
                        <span className="text-sm">Kode Pos</span>
                        <Input placeholder="Kode Pos" />
                    </div>
                    <div className="">
                        <span className="text-sm">Provinsi</span>
                        <Input placeholder="Pilih" />
                    </div>
                    <div className="">
                        <span className="text-sm">Kota/Kabupaten</span>
                        <Input placeholder="Pilih" />
                    </div>
                    <div className="">
                        <span className="text-sm">Kecamatan</span>
                        <Input placeholder="Pilih" />
                    </div>
                </div>
            </div>
            <div className="border-b-2 pb-6 mt-6">
                <h1 className="text-black font-medium text-2xl">Data Pendidikan</h1>
                <div className="grid grid-cols-3 gap-4 mt-6 text-black">
                    <div className="">
                        <span className="text-sm">Pendidikan Terakhir</span>
                        <Input placeholder="Pilih" />
                    </div>
                    <div className="">
                        <span className="text-sm">Nama Sekolah</span>
                        <Input placeholder="Masukan Nama Sekolah" />
                    </div>
                    <div className="">
                        <span className="text-sm">Rata-rata Nilai Rapor Kelas 12</span>
                        <Input placeholder="Masukan Rata-rata Nilai Rapor" />
                    </div>
                </div>
            </div>
            <button className="py-4 px-6 bg-mainblue text-white rounded mt-6 focus:outline-none">Simpan</button>
        </div>
    )
}
