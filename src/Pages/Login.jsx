import React,{ useState } from 'react'
import { FiEye,FiEyeOff,FiXCircle } from "react-icons/fi";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers';
import * as yup from "yup";
import { Link, useHistory } from "react-router-dom";
import Button from "../Components/Button"
import Loading from '../Components/Loading';
import Swal from 'sweetalert2'
/* eslint-disable */

const axios = require('axios');

export default function Login() {
    let history = useHistory();



    const schema = yup.object().shape({
        email: yup.string().required(),
        password: yup.string().required().min(6),
    });
    const [showPass, setShowPass] = useState(false)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState({})


    const { register, handleSubmit, errors,setValue,watch } = useForm({
        resolver: yupResolver(schema)
    });

      
    const watchField = watch(undefined, { 
        email: '',
        password: '',
    })

    const onSubmit = (data) => {
            
        // return history.push('/fe');
        setLoading(true)
        setError({message : ''})
        axios.post(`${process.env.REACT_APP_API_URL}auth`, {
            username: data.email,
            password: data.password
        })
        .then((response)=> {
            if (!response.data.isValid) {
                return setError({message : response.data.message})
            }
            localStorage.setItem('token',response.data.rowData)
            history.push('/fe');
        })
        .catch((error)=>{
            Swal.fire(
                'Oopss!',
                'Something when wrong!',
                'error'
              )
        }).then(()=>{
            setLoading(false)
        });

    }

    return (
        <div className="relative">
        <Loading visible={loading}/>
            <div style={{backgroundColor:'#F5F6FA'}} className=" h-screen  overflow-hidden relative">
                <img src={require('../assets/img/loginbottom.svg')} alt="loginbottom" className="absolute bottom-0 w-full -mb-24 lg:-mb-40 -sm:hidden"/>
                <div className="relative z-0 flex flex-col justify-center items-center mt-48 px-4 md:px-8">
                    <img src={require('../assets/img/logo.png')} alt="logo"/>
                    <h1 style={{color:'#575757'}} className="font-light text-2xl  text-center">Login to access Dashboard</h1>
                    <p className="text-center text-red my-5" >{error.message}</p>
                    <form onSubmit={handleSubmit(onSubmit)} className="w-full flex flex-col justify-center items-center">
                        {/* <input style={{color:'#686868'}} placeholder="email@uin.com" type="text" className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4 focus:outline-none px-4 py-4 border-b"/> */}
                        <div className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4">
                            <input ref={register} type="text" id="email" name="email" placeholder="jhon@mail.com" className={`w-full px-4 py-4 border-b focus:outline-none ${errors.email ? "border border-red" : ''}`}/>
                            <p className={`text-xs text-red ${errors.email ? "mb-2":""}`} >{errors.email?.message}</p>
                        </div>
                        <div className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4">
                            <div className={`flex items-center w-full bg-white px-4 py-4 ${errors.password ? "border border-red" : ''}`}>
                                <input style={{color:'#686868'}} ref={register} type={showPass ? 'text' : 'password'} id="password" name="password" placeholder="*******" className={`w-full bg-transparent focus:outline-none pr-2`}/>
                                {watchField.password !== '' ? <FiXCircle onClick={()=>{setValue('password','')}} className="mr-2"/> : null}
                                {showPass ? <FiEye className={`${watchField.password === '' ? 'text-gray' : ''}`} onClick={()=>{setShowPass(!showPass)}} /> : <FiEyeOff className={`${watchField.password === '' ? 'text-gray' : ''}`} onClick={()=>{setShowPass(!showPass)}} />}
                            </div>
                            <p className={`text-xs text-red ${errors.email ? "mb-2":""}`} >{errors.password?.message}</p>
                        </div>
                        <Button className="bg-mainblue mt-6 w-full md:w-1/2 lg:w-1/3 xl:w-1/4">Sign In</Button>
                        <div className="flex justify-between items-center mt-6 w-full md:w-1/2 lg:w-1/3 xl:w-1/4">
                            <label style={{color:'#686868'}} className="flex items-center -sm:text-sm">
                                <input type="checkbox" className="form-checkbox text-mainblue text-xl"/>
                                <span className="ml-2">Remember Me</span>
                            </label>
                            <Link to="/forgotpassword" className="hover:underline -sm:text-sm text-mainblue">Forgot Password?</Link>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
