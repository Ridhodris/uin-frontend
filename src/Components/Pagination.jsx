import React from 'react'
import ReactPaginate from 'react-paginate';

export default function Pagination({page,lastPage,pageCount,onPageChange}) {
    return (
        <ReactPaginate 
            activeLinkClassName="bg-mainblue text-white" 
            pageClassName="bg-white w-10 flex items-center justify-center rounded overflow-hidden" 
            pageLinkClassName="p-2 text-center focus:outline-none hover:bg-mainblue hover:text-white select-none w-full" 
            previousLinkClassName={`focus:outline-none p-2 w-full text-center rounded ${page ===0   ? 'bg-gray cursor-not-allowed' : 'bg-white hover:bg-mainblue hover:text-white'} select-none`} 
            nextLinkClassName={`focus:outline-none p-2 w-full text-center rounded  ${page +1 === lastPage ? 'bg-gray cursor-not-allowed' : 'bg-white hover:bg-mainblue hover:text-white'} select-none`} 
            nextClassName="w-24 flex items-center justify-center" 
            previousClassName="w-24 flex items-center justify-center"  containerClassName="flex items-center space-x-2 mt-4" 
            marginPagesDisplayed={2} 
            pageRangeDisplayed={5} 
            pageCount={pageCount} 
            forcePage={page} 
            onPageChange={(data)=>{onPageChange(data.selected)}} 
        />
    )
}
