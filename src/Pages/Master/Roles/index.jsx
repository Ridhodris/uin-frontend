import React from 'react'
import { FiCloudOff, FiEdit,  FiSearch,  FiTrash, FiUsers } from 'react-icons/fi';
import MyModal from "../../../Components/MyModal"
import Input from "../../../Components/Input"
import Button from "../../../Components/Button"
import Loading from '../../../Components/Loading';
import {debounce} from 'lodash';
import Pagination from '../../../Components/Pagination';
import AlertError from "../../../Components/AlertError"
import TreeSelectWrapper from "../../../Components/TreeSelectWrapper"
import MySelect from '../../../Components/MySelect';
const axios = require('axios');

/* eslint-disable */

export default class Roles extends React.Component {
    state = {
        modalIsOpen: false,
        loading    : false,
        alert      : false,
        alertError : false,
        dataApplicationRole : [],
        treeSelected : [],
        AllRoles   : [],
        filter     : {
            search   : ''
        },
        paginate : {
            perPage  : 5,
            page     : 0,
            lastPage : 0,
            pageCount: 0,
        },
        data         : {
            // roleId  : '',
            roleCode: '',
            roleName: '',
            subroleList :[]
        },
        modalRoleMapping      : false,
        allChecked      : false,
        setRole         : 0,
        filterRole      : 0,
        filterGroup     : 0,
        dataRole        : [],
        dataGroup       : [],
        dataUsers       : []
    };
    
    componentDidMount(){
        Promise.all([this.getRole(),this.getGroup(),this.getRoleApps(),this.getData()])
    }

    handleChange = (e) =>{
        this.setState({
            data : {
                ...this.state.data,
                [e.target.name] : e.target.value
            }
        })
    }
    onChange = (currentNode, selectedNodes) => {

        var appList = []
        var subroleList = []
        selectedNodes.filter(x => x._depth > 0 ).map((e)=>subroleList.push({subroleId:e.value}))
        selectedNodes.filter(x => x._depth < 1 ).map((e)=>appList.push({value:e.value}))
        var temp = []
        this.state.dataApplicationRole.map((item)=>{
            return appList.map(element => {
                if (item.value === element.value) {
                    return item.children.map((e)=>temp.push({subroleId:e.value}))
                }
            });
          });
        subroleList = subroleList.concat(temp)
        this.setState({
            data : {
                ...this.state.data,
                subroleList:subroleList
            }
        })
    }

    getRoleApps = async () => {

        return await fetch(`${process.env.REACT_APP_API_URL}app/appRoleApp`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            var AppTemp = []
            response.map((e)=>AppTemp.push({value:e.idApp,label:e.appName}))
            AppTemp = AppTemp.filter( (ele, ind) => ind === AppTemp.findIndex( elem => elem.value === ele.value && elem.label === ele.label))
            var appRole = []
            AppTemp.map((e,i)=>{
                var children = []
                response.filter(x => x.idApp === e.value).map((e)=>children.push({value:e.idRole,label:e.roleName}))
                return appRole.push({
                    ...e,
                    children:children
                })
            })
            // console.log(appRole)
            this.setState({
                dataApplicationRole : appRole,
                loading : false,
            })
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                loading : false,
            })
        });
    }

    handleFilter = debounce((search) => {
        this.setState({
            filter : {
                search : search
            }
        },()=>{
            this.getData()
        })
    },500)

    getData = async () => {
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}master-role?search=${this.state.filter.search}&page=${this.state.paginate.page}&size=${this.state.paginate.perPage}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            this.setState({
                AllRoles : response.list,
                // modalIsOpen : false,
                alertError : false,
                loading : false,
                paginate : {
                    ...this.state.paginate,
                    page : response.pageable.page,
                    lastPage : response.pageable.totalPages,
                    pageCount:response.pageable.totalPages,
                }
            })
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                loading : false,
            })
        });
    }

    handleAdd = () =>{
        this.setState({
            modalIsOpen : true,
            data : {
                // roleId : '',
                roleCode : '',
                roleName : '',
                status : 1,
                subroleList :[]
            }
        })
    }

    handleDelete = async (roleId) =>{
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}master-role/${roleId}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(()=>{
            this.setState({
                paginate : {
                    ...this.state.paginate,
                    page : 0
                }
            },()=>{
                this.getData()
                this.setState({
                    loading:false,
                    alert:false
                })
            })
        }).catch(err=>{
            console.log(err)
        });
    }

    handleEdit = async (roleId) => {
        // this.setState({loading:true})
        const Haeder =  await fetch(`${process.env.REACT_APP_API_URL}master-role/${roleId}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        })
        const Detail =  await fetch(`${process.env.REACT_APP_API_URL}master-role/${roleId}/subrole`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        })
        const responHeader = await Haeder.json()
        const responDetail = await Detail.json()
        var detail = []
        var temp = this.state.dataApplicationRole
        responDetail.list.map((e)=>detail.push({subroleId:e.subroleId}))
        this.state.dataApplicationRole.map((e,ei)=>{
            e.children.map((a,ai)=>{
                detail.map((d)=>{
                    if (a.value === d.subroleId) {
                        temp[ei].checked = true
                        temp[ei].children[ai].checked = true
                    }
                })
            })
        })
        this.setState({
            dataApplicationRole : temp,
            modalIsOpen : true
        })
        // console.log(temp)
    }
    handleSave = async () =>{
        // return console.log(this.state.data)
        this.setState({loading:true})
        const method = this.state.data.roleId ? 'PUT':'POST'
        return await fetch(`${process.env.REACT_APP_API_URL}master-role${this.state.data.roleId ? '/'+this.state.data.roleId : '' }`, {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
            body: JSON.stringify(this.state.data)
        }).then(()=>{
            this.getData()
            this.setState({modalIsOpen:false, loading : false,alertError : false})
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                loading : false,
            })
        });
    }

    handlePageChange = (selected) => {
        this.setState({ 
            paginate : {
                ...this.state.paginate,
                page: selected
            }
         }, () => {
          this.getData();
        });
    };

    getRole = async () => {
        return await fetch(`${process.env.REACT_APP_API_URL}master-role`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{

            let roleTemp = []
            response.list.map((e)=>roleTemp.push({label:e.roleName,value:e.id}))

            this.setState({
                dataRole : roleTemp,
                loading : false,
            })
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                loading : false,
            })
        });
    }

    getGroup = async () => {
        return await fetch(`${process.env.REACT_APP_API_URL}group`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            let groupTemp = []
            response.list.map((e)=>groupTemp.push({label:e.groupName,value:e.id}))
            this.setState({
                dataGroup : groupTemp,
                loading : false,
            })
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                loading : false,
            })
        });
    }

    getUserMapping =  async () => {
        this.setState({loading:true})
        await axios.get(`${process.env.REACT_APP_API_URL}role/map?size=1000000`,{
            headers: {
                'SSO-TOKEN' : localStorage.getItem('token')
            }
        })
        .then((response)=> {
            var data = []
            response.data.list.map((e)=> {return data.push({id:e.id,userUniqueId:e.userUniqueId,isChecked:false})})
            this.setState({dataUsers:data,allChecked:false})
        })
        .catch((error)=>{
            console.log(error.response.data)
        }).then(()=>{
            this.setState({loading:false})
        });
    }

    handleCheked = (e) => {
        let itemName = e.target.name;
        let checked = e.target.checked;
        this.setState(prevState => {
        let { dataUsers, allChecked } = prevState;
        if (itemName === "checkAll") {
            allChecked = checked;
            dataUsers = dataUsers.map(item => ({ ...item, isChecked: checked }));
        } else {
            dataUsers = dataUsers.map(item =>
            item.userUniqueId === itemName ? { ...item, isChecked: checked } : item
            );
            allChecked = dataUsers.every(item => item.isChecked);
        }
        return { dataUsers, allChecked };
        });
    }

    handleUnCheked = (e) => {
        this.setState(prevState => {
            let { dataUsers } = prevState;
            dataUsers = dataUsers.map(item =>
            item.userUniqueId === e ? { ...item, isChecked: false } : item
            );
            return { dataUsers};
            });
    }

    handleRoleMappingSave = async () => {
        let data = []
        this.state.dataUsers.filter(x => x.isChecked === true).map((e)=>{
            return data.push({userUniqueId:e.userUniqueId})
        })
        let datas = {
            roleId : this.state.setRole,
            userList : data
        }
        this.setState({loading:true})
        await axios.put(`${process.env.REACT_APP_API_URL}role/map`,{...datas},
        {
            headers: {
                'SSO-TOKEN' : localStorage.getItem('token')
            }
        })
        .then((response)=> {
           
            this.getUserMapping()
        })
        .catch((error)=>{
            console.log(error.response.data)
        }).then(()=>{
            this.setState({loading:false})
        });
    }

    handleSelectChange = (newValue, selectedOptions) => {
        if (selectedOptions.action === "clear") {
            this.setState({
                [selectedOptions.name] : 0
            },()=>{
                if (this.state.filterRole === 0 && this.state.filterGroup === 0) {
                    this.getUserMapping()
                }
            })
        }else{
            this.setState({
                [selectedOptions.name] : newValue.value
            },async ()=>{
                if (this.state.filterRole !== 0 && this.state.filterGroup !== 0) {
                    this.setState({loading:true})
                    await axios.get(`${process.env.REACT_APP_API_URL}role/${this.state.filterGroup}/${this.state.filterRole}/map`,{
                        headers: {
                            'SSO-TOKEN' : localStorage.getItem('token')
                        }
                    })
                    .then((response)=> {
                        if (response.data.length > 0) {
                            var data = []
                            response.data.list.map((e)=>data.push({id:e.id,userUniqueId:e.userUniqueId,isChecked:false}))
                            this.setState({dataUsers:data})
                        }else{
                            this.setState({dataUsers:[]})
                        }
                    })
                    .catch((error)=>{
                        console.log(error)
                    }).then(()=>{
                        this.setState({loading:false})
                    });
                }
            })
        }
    }

    render(){
        return (
            <div className="px-4">
                <Loading visible={this.state.loading}/>
                <div className="flex items-center justify-between space-x-4 flex-1">
                    <h1 className="text-black text-2xl font-medium">Master Role</h1>
                    <div className="flex items-center space-x-5">
                        <Button onClick={()=>{Promise.all([this.getRole(),this.getGroup(),this.getUserMapping()]);this.setState({modalRoleMapping:true})}} className="bg-mainblue">
                            <span>Role Mapping</span>
                        </Button>
                        <button onClick={()=>{this.handleAdd()}} className="py-4 bg-mainblue focus:outline-none rounded px-6 flex items-center space-x-2 text-white">
                            <span>Add Role</span>
                        </button>
                    </div>
                </div>
                <div className="my-4 flex items-center justify-between">
                    <div className="flex items-center space-x-4 text-mainblue">
                        <FiUsers className="text-xl cursor-pointer text-mainblue"/>
                        <span>{this.state.AllRoles.length} Active Roles</span>
                    </div>
                    <div className="w-20p">
                        <Input name="search" onChange={(e)=>{this.handleFilter(e.target.value)}} icon={<FiSearch/>} placeholder="Search" />
                    </div>
                </div>
                <div className="w-full">
                    <table className="w-full bg-white rounded-lg">
                        <thead className="border-b text-left text-black">
                            <tr>
                                <th className="py-4 px-4">No</th>
                                <th>Role Code</th>
                                <th>Role Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.AllRoles.length === 0 ? (
                                    <tr>
                                        <td colSpan="4" className="py-8 px-4">
                                            <div className="flex items-center space-x-4 justify-center w-full text-center text-gray">
                                                <FiCloudOff/>
                                                <p>No data</p>
                                            </div>
                                        </td>
                                    </tr>
                                ) :(null)
                            }
                            {
                                this.state.AllRoles.map((role,key)=>{
                                    return(
                                        <tr key={key} className="hover:bg-table odd:bg-table">
                                            <td className="py-3 px-4">{key+1}</td>
                                            <td>{role.roleCode}</td>
                                            <td>{role.roleName}</td>
                                            <td>
                                                <div className="flex items-center space-x-2">
                                                    <FiEdit onClick={()=>{this.handleEdit(role.id)}} className="cursor-pointer"/>
                                                    <FiTrash onClick={()=>{this.setState({alert:true,data:{roleId:role.id}})}} className="cursor-pointer"/>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            
                        </tbody>
                    </table>
                </div>
                <Pagination 
                    onPageChange={(selected)=>{this.handlePageChange(selected)}} 
                    lastPage={this.state.paginate.lastPage} 
                    page={this.state.paginate.page} 
                    pageCount={this.state.paginate.pageCount} 
                />
                <AlertError show={this.state.alertError} close={()=>{this.setState({alertError:false})}} OnRefresh={()=>{this.state.modalIsOpen ? this.handleSave() : this.getData()}} />
                <MyModal md modalIsOpen={this.state.alert} setModal={()=>{this.setState({alert:false})}}>
                    <div className="flex justify-center flex-col items-center font-Poppins">
                        <h1 className="text-xl font-bold">Are you sure ?</h1>
                        <p>Do you want to delete this role?</p>
                        <div className="flex items-center space-x-2 mt-8">
                            <button onClick={()=>{this.handleDelete(this.state.data.roleId)}} className="p-4 focus:outline-none w-24 bg-red text-white rounded" >Yes</button>
                            <button onClick={()=>{this.setState({alert:false})}} className="p-4 focus:outline-none w-24 border text-black rounded" >Cancel</button>
                        </div>
                    </div>
                </MyModal>
                <MyModal modalIsOpen={this.state.modalIsOpen} setModal={()=>{this.setState({modalIsOpen:false})}}>
                    <div className="flex justify-between items-center font-Poppins">
                        <h1 className="text-xl font-bold">Create Role</h1>
                    </div>
                    <form  onSubmit={(e)=>{e.preventDefault();this.handleSave()}} className="mt-4 font-Poppins flex flex-col space-y-3">
                        <div className="flex items-center gap-3">
                            <Input label="Role Code" name="roleCode" onChange={(e)=>{this.handleChange(e)}} value={this.state.data.roleCode} placeholder="Role Code" />
                            <Input label="Role Name" name="roleName" onChange={(e)=>{this.handleChange(e)}} value={this.state.data.roleName} placeholder="Role Name" />
                        </div>
                        <div className="w-full">
                            <span  className={`text-sm text-black font-medium`}>Apps Role</span>
                            <TreeSelectWrapper onChange={this.onChange} data={this.state.dataApplicationRole} />
                        </div>
                        {/* <MySelect data={this.state.dataRole} label="Application Role" placeholder="Select Application Role"/> */}
                        <div className="col-span-2">
                            <Button className="bg-mainblue float-right">{this.state.data.id ? 'Update' : 'Save'}</Button>
                        </div>
                    </form>
                </MyModal>
                <MyModal modalIsOpen={this.state.modalRoleMapping} setModal={()=>{this.setState({modalRoleMapping:false})}}>
                    <div className="flex justify-center items-center font-Poppins">
                        <h1 className="text-xl font-bold text-center">Role Mapping</h1>
                    </div>
                    <div className="mt-8 flex gap-5">
                        <div className="w-full">
                            <div className="grid grid-cols-2 gap-5 mt-8">
                                <MySelect data={this.state.dataGroup} name="filterGroup"  onChange={(a,b)=>this.handleSelectChange(a,b)}  label="Filter Group" placeholder="Select Group"/>
                                <MySelect data={this.state.dataRole} name="filterRole"  onChange={(a,b)=>this.handleSelectChange(a,b)}  label="Filter Role" placeholder="Select Role"/>
                            </div>
                            <div className="h-50vh overflow-y-auto mt-6 border-t">
                                <table className="w-full bg-white rounded-lg">
                                    <thead className="border-b text-left text-black">
                                        <tr>
                                            <th className="py-4 px-4 w-10">
                                            <input  checked={this.state.allChecked} name="checkAll"  onChange={(e)=>this.handleCheked(e)} type="checkbox" className="form-checkbox text-mainblue text-xl"/>
                                            </th>
                                            <th>No</th>
                                            <th>User ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataUsers.map((e,i)=>{
                                            return(
                                                <tr key={i} className="hover:bg-table odd:bg-table">
                                                    <td className="py-3 px-4">
                                                        <input type="checkbox" name={e.userUniqueId} onChange={(e)=>this.handleCheked(e)} checked={e.isChecked} value={e.userUniqueId} className="form-checkbox text-mainblue text-xl"/>
                                                    </td>
                                                    <td> {i+1} </td>
                                                    <td> {e.userUniqueId} </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="w-full">
                            <div className="mt-8">
                                <MySelect data={this.state.dataRole} label="Set Role" name="setRole" onChange={(a,b)=>this.handleSelectChange(a,b)} placeholder="Select Role"/>
                            </div>
                            <div className="h-50vh overflow-y-auto mt-6 border-t">
                                <table className="w-full bg-white rounded-lg">
                                    <thead className="border-b text-left text-black">
                                        <tr>
                                            <th className="py-4 px-4">No</th>
                                            <th>User ID</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.dataUsers.filter(x => x.isChecked === true).map((e,i)=>{
                                            return(
                                                <tr key={i} className="hover:bg-table odd:bg-table">
                                                    <td className="py-3 px-4">{i+1}</td>
                                                    <td> {e.userUniqueId} </td>
                                                    <td>
                                                        <div className="flex justify-center items-center space-x-2">
                                                            <FiTrash onClick={()=>this.handleUnCheked(e.userUniqueId)} className="cursor-pointer"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <Button onClick={()=>this.handleRoleMappingSave()} className="bg-mainblue mt-5 float-right">Save</Button>
                </MyModal>
            </div>
        )
    }
}
