import React from 'react'
import { FiUsers,FiPlus } from "react-icons/fi";
import {groupsData} from "../../Data/Groups"
import {usersData} from "../../Data/Users"
import { FaTrash } from "react-icons/fa";
import { MdModeEdit } from "react-icons/md";
import { AiOutlineEye, AiOutlineSearch } from "react-icons/ai";
import Input from "../../Components/Input"
import MyModal from "../../Components/MyModal"
import Button from '../../Components/Button';
import Select from 'react-select'


export default function Groups() {
    const [modalAddIsOpen,setmodalAddIsOpen] = React.useState(false);
    const [modalShowIsOpen,setmodalShowIsOpen] = React.useState(false);
    const [modalEditIsOpen,setmodalEditIsOpen] = React.useState(false);
    const [search,setSearch] = React.useState('');
    return (
        <div className="p-10">
            <div className="flex justify-between items-center">
                <div>
                    <h1 className="font-bold text-2xl">Groups</h1>
                    <div className="flex items-center space-x-8 mt-4">
                        <div className="flex items-center">
                            <div className="bg-mainblue bg-opacity-25 rounded-full w-10 h-10 flex justify-center items-center">
                                <FiUsers className="text-mainblue font-bold"/>
                            </div>
                            <h1 className="font-semibold ml-3">{groupsData.length} Active Roles</h1>
                        </div>
                    </div>
                </div>
                <div onClick={()=>{setmodalAddIsOpen(true)}} className="flex justify-center items-center w-12 h-12 bg-mainblue rounded-full shadow-blue cursor-pointer hover:bg-opacity-75 group">
                    <FiPlus className="text-white text-3xl group-hover:text-mainblue"/>
                </div>
            </div>
            <div className="mt-8 overflow-x-auto min-w-full">
                <table style={{borderCollapse:"separate",borderSpacing:"0 .75rem"}} className="min-w-full">
                    <thead>
                        <tr className="text-left bg-input text-mainblue rounded-md">
                            <th className="px-4 whitespace-no-wrap py-4 rounded-l-md">No</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Groups Code</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Groups Name</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Users</th>
                            <th className="-lg:px-4 whitespace-no-wrap rounded-r-md text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            groupsData.map((data,key)=>{
                                return(
                                    <tr key={key} className="text-left bg-white text-black rounded-md font-semibold hover:bg-mainblue hover:bg-opacity-25 hover:text-mainblue">
                                        <td className="px-4 py-4 rounded-l-md">{key+1}</td>
                                        <td className="-lg:px-4">{data.groupCode}</td>
                                        <td className="-lg:px-4">{data.groupName}</td>
                                        <td className="-lg:px-4 text-center cursor-pointer">
                                            <AiOutlineEye onClick={()=>{setmodalShowIsOpen(true)}} />
                                        </td>
                                        <td className="-lg:px-4 rounded-r-md text-center">
                                            <div className="flex space-x-2 items-center justify-center">
                                                <MdModeEdit onClick={()=>{setmodalEditIsOpen(true)}} size={20} className="cursor-pointer text-black"/>
                                                <FaTrash size={20} className="text-red cursor-pointer"/>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                
            </div>
            <MyModal modalIsOpen={modalAddIsOpen} setModal={()=>{setmodalAddIsOpen(false)}}>
                <div className="flex justify-between items-center font-Poppins">
                    <h1 className="text-xl font-bold">Create Groups</h1>
                </div>
                <div className="mt-4 font-Poppins flex flex-col space-y-5">
                    <Input placeholder="Group Code" />
                    <Input placeholder="Group Name" />
                    <Select 
                        placeholder="Select Users"
                        closeMenuOnSelect={false}
                        isMulti 
                        menuPortalTarget={document.body}  
                        options={usersData}
                        getOptionLabel={(option)=>option.userid +' || '+ option.username}
                        getOptionValue={(option)=>option.id} 
                        styles={{ 
                            control:(provided, state)=>({
                                // ...provided,
                                display:'flex',
                                padding: '0.75rem',
                                backgroundColor: state.isFocused ? 'white': '#EBEBF2',
                                borderRadius:'0.375rem',
                                borderWidth:'2px',
                                borderColor: !state.isFocused ? 'transparent' : '#A4A4BA'
                            }),
                            multiValue:(provided)=>({
                                ...provided,
                                backgroundColor : 'hsl(192deg 62% 85%)'
                            }),
                            option:(provided,state)=>({
                                ...provided,
                                backgroundColor : !state.isFocused ? 'white' : 'hsl(192deg 62% 85%)'
                            })
                         }}
                    />
                    <div className="col-span-2">
                        <Button onClick={()=>{setmodalAddIsOpen(false)}} className="bg-mainblue float-right">Save</Button>
                    </div>
                </div>
            </MyModal>
            <MyModal lg modalIsOpen={modalShowIsOpen} setModal={()=>{setmodalShowIsOpen(false)}}>
                <Input value={search} onChange={(e)=>{setSearch(e.target.value)}} placeholder="Search.." className="mb-3">
                    <AiOutlineSearch className="text-2xl text-black"/>
                </Input>
                <table className="min-w-full">
                    <thead>
                        <tr className="text-left bg-input text-mainblue rounded-md">
                            <th className="px-4 whitespace-no-wrap py-4 rounded-l-md">No</th>
                            <th className="-lg:px-4 whitespace-no-wrap">User Id</th>
                            <th className="-lg:px-4 whitespace-no-wrap">User Name</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Full Name</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Email</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            usersData.slice(0,5).map((data,key)=>{
                                return(
                                    <tr key={key} className="text-left bg-white text-black rounded-md font-semibold hover:bg-mainblue hover:bg-opacity-25 hover:text-mainblue">
                                        <td className="px-4 py-3 rounded-l-md">{key+1}</td>
                                        <td className="-lg:px-4">{data.userid}</td>
                                        <td className="-lg:px-4">{data.username}</td>
                                        <td className="-lg:px-4">{data.fullname}</td>
                                        <td className="-lg:px-4">{data.email}</td>
                                        <td className={`-lg:px-4 ${data.status === 'Active' ? 'text-green' : 'text-red'} `}>{data.status}</td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </MyModal>
            <MyModal full modalIsOpen={modalEditIsOpen} setModal={()=>{setmodalEditIsOpen(false)}}>
                <Input value={search} onChange={(e)=>{setSearch(e.target.value)}} placeholder="Search.." className="mb-3">
                    <AiOutlineSearch className="text-2xl text-black"/>
                </Input>
                <div className="flex justify-between space-x-3 items-center">
                <div className="w-full">
                    <Select 
                        placeholder="Select Users"
                        closeMenuOnSelect={false}
                        isMulti 
                        menuPortalTarget={document.body}  
                        options={usersData}
                        getOptionLabel={(option)=>option.userid +' || '+ option.username}
                        getOptionValue={(option)=>option.id} 
                        styles={{ 
                            control:(provided, state)=>({
                                // ...provided,
                                display:'flex',
                                padding: '0.75rem',
                                backgroundColor: state.isFocused ? 'white': '#EBEBF2',
                                borderRadius:'0.375rem',
                                borderWidth:'2px',
                                borderColor: !state.isFocused ? 'transparent' : '#A4A4BA'
                            }),
                            multiValue:(provided)=>({
                                ...provided,
                                backgroundColor : 'hsl(192deg 62% 85%)'
                            }),
                            option:(provided,state)=>({
                                ...provided,
                                backgroundColor : !state.isFocused ? 'white' : 'hsl(192deg 62% 85%)'
                            })
                            }}
                        />
                </div>
                    <Button className="bg-mainblue">Add</Button>
                </div>
            <table style={{borderCollapse:"separate",borderSpacing:"0 .75rem"}} className="min-w-full">
                    <thead>
                        <tr className="text-left bg-input text-mainblue rounded-md">
                            <th className="px-4 whitespace-no-wrap py-4 rounded-l-md">No</th>
                            <th className="-lg:px-4 whitespace-no-wrap">User Id</th>
                            <th className="-lg:px-4 whitespace-no-wrap">User Name</th>
                            <th className="-lg:px-4 whitespace-no-wrap">Email</th>
                            <th className="-lg:px-4 whitespace-no-wrap rounded-r-md text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            usersData.slice(0,5).map((data,key)=>{
                                return(
                                    <tr key={key} className="text-left bg-white text-black rounded-md font-semibold hover:bg-mainblue hover:bg-opacity-25 hover:text-mainblue">
                                        <td className="px-4 py-4 rounded-l-md">{key+1}</td>
                                        <td className="-lg:px-4">{data.userid}</td>
                                        <td className="-lg:px-4">{data.username}</td>
                                        <td className="-lg:px-4">{data.email}</td>
                                        <td className="-lg:px-4 rounded-r-md text-center">
                                            <div className="flex space-x-2 items-center justify-center">
                                                <FaTrash size={20} className="text-red cursor-pointer"/>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </MyModal>
        </div>
    )
}
