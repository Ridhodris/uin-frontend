import React from 'react'

export default function Table() {
    return (
    <table className="min-w-full">
        <thead>
            <tr className="text-left bg-input text-mainblue rounded-md">
                <th className="px-4 whitespace-no-wrap py-4 rounded-l-md">No</th>
                <th className="-lg:px-4 whitespace-no-wrap">User Id</th>
                <th className="-lg:px-4 whitespace-no-wrap">User Name</th>
                <th className="-lg:px-4 whitespace-no-wrap">Full Name</th>
                <th className="-lg:px-4 whitespace-no-wrap">Email</th>
                <th className="-lg:px-4 whitespace-no-wrap">Status</th>
            </tr>
        </thead>
        <tbody>
            {
                usersData.slice(0,5).map((data,key)=>{
                    return(
                        <tr key={key} className="text-left bg-white text-black rounded-md font-semibold hover:bg-mainblue hover:bg-opacity-25 hover:text-mainblue">
                            <td className="px-4 py-3 rounded-l-md">{key+1}</td>
                            <td className="-lg:px-4">{data.userid}</td>
                            <td className="-lg:px-4">{data.username}</td>
                            <td className="-lg:px-4">{data.fullname}</td>
                            <td className="-lg:px-4">{data.email}</td>
                            <td className={`-lg:px-4 ${data.status === 'Active' ? 'text-green' : 'text-red'} `}>{data.status}</td>
                        </tr>
                    )
                })
            }
        </tbody>
    </table>
    )
}
