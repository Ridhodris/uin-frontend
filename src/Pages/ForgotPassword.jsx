import React, { useState } from 'react'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers';
import * as yup from "yup";
import { Link, useHistory } from "react-router-dom";
import Button from "../Components/Button"
import Loading from '../Components/Loading';
import Swal from 'sweetalert2'

const axios = require('axios');

export default function ForgotPassword() {
    let history = useHistory();



    const schema = yup.object().shape({
        email: yup.string().email().required(),
    });
    const [loading, setLoading] = useState(false)


    const { register, handleSubmit, errors } = useForm({
        resolver: yupResolver(schema)
    });


    const onSubmit = (data) => {
        // console.log(data)
        setLoading(true)
        axios.post(`${process.env.REACT_APP_API_URL}forgot-password`, {
            mail: data.email,
        })
        .then((response)=> {
            // localStorage.setItem('token',response.data.rowData)
            // history.push('/checkyouremail');
            Swal.fire({
                title: 'Email has ben sent!',
                text: "Please check your inbox",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#029CC3',
                confirmButtonText: 'Log In'
              }).then((result) => {
                if (result.isConfirmed) {
                    return history.push('/checkyouremail');
                }
              })
        })
        .catch((error)=>{
            // console.log()
            Swal.fire(
                'Oopss!',
                error.response.data.message,
                'error'
              )
        }).then(()=>{
            setLoading(false)
        });
    }

    return (
        <div className="relative">
        <Loading visible={loading}/>
            <div style={{backgroundColor:'#F5F6FA'}} className=" h-screen  overflow-hidden relative">
                <img src={require('../assets/img/loginbottom.svg')} alt="loginbottom" className="absolute bottom-0 w-full -mb-24 lg:-mb-40 -sm:hidden"/>
                <div className="relative z-0 flex flex-col justify-center items-center mt-48 px-4 md:px-8">
                    <img src={require('../assets/img/logo.png')} alt="logo"/>
                    <h1 style={{color:'#575757'}} className="font-light text-2xl  text-center mt-2">Forgot your password ?</h1>
                    <p className="text-gray w-full md:w-1/2 lg:w-1/3 xl:w-1/4 text-center text-sm">Submit your email address and we'll send you a link to reset your password</p>
                    <form onSubmit={handleSubmit(onSubmit)} className="w-full flex flex-col mt-5 justify-center items-center">
                        {/* <input style={{color:'#686868'}} placeholder="email@uin.com" type="text" className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4 focus:outline-none px-4 py-4 border-b"/> */}
                        <div className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4">
                            <input ref={register} type="text" id="email" name="email" placeholder="your@mail.com" className={`w-full px-4 py-4 focus:outline-none ${errors.email ? "border border-red" : ''}`}/>
                            <p className={`text-xs text-red ${errors.email ? "mb-2":""}`} >{errors.email?.message}</p>
                        </div>
                        <Button className="bg-mainblue mt-6 w-full md:w-1/2 lg:w-1/3 xl:w-1/4">Submit</Button>
                        <p className="text-sm text-gray mt-6">Did you remember your password? <Link className="text-mainblue hover:underline cursor-pointer" to="/login">Try Sign In</Link></p>
                    </form>
                </div>
            </div>
        </div>
    )
}
