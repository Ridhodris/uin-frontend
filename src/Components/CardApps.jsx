import React from 'react'
import {FiCodesandbox, FiEdit, FiTrash } from "react-icons/fi";
import { BsThreeDots, BsToggleOff, BsToggleOn } from "react-icons/bs";
import { useState } from 'react';

export default function CardApps({data,deletes,update,active}) {
    const [show, setShow] = useState(false)
    return (
        <div className="bg-white rounded-lg py-6 px-8 relative">
            <div className="flex justify-between items-start ">
                <FiCodesandbox className="text-5xl"/>
                <div className="flex items-center space-x-4">
                    <h1 className={`${data.status !== 1 ? 'text-red' :'text-green'} `}>{data.status === 1 ? 'Active' : 'InActive'}</h1>
                    <BsThreeDots onClick={()=>{setShow(!show)}} className="text-xl cursor-pointer"/>
                    <div style={{right:'-32px'}} className={`absolute top-0 z-10 p-2 rounded bg-white shadow-outline mt-12 overflow-hidden ${!show && 'hidden'}`}>
                        <ul>
                            <li onClick={()=>{setShow(false);update()}} className="px-2 py-1 cursor-pointer hover:bg-mainblue hover:text-white flex items-center space-x-2">
                                <FiEdit/>
                                <span>Update</span>
                            </li>
                            <li onClick={()=>{active()}} className="px-2 py-1 cursor-pointer hover:bg-mainblue hover:text-white flex items-center space-x-2">
                                {data.status === 1 ? <BsToggleOff/> : <BsToggleOn/>}
                                
                                <span>{data.status === 1 ? 'Active' : 'InActive'}</span>
                            </li>
                            <li  onClick={()=>{setShow(false);deletes()}} className="px-2 py-1 cursor-pointer hover:bg-mainblue hover:text-white flex items-center space-x-2 text-red"><FiTrash/><span>Delete</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="mt-6">
                <h1 className="font-medium text-2xl">{data.appName}</h1>
                <span className="text-sm">{data.description}</span>
            </div>
        </div>
    )
}
