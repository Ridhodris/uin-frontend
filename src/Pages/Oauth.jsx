import React, { Component } from 'react';
import Button from '../Components/Button'
import Input from '../Components/Input'
export default class Oauth extends Component {
  render() {
    return (
      <div className="h-screen bg-main flex items-center justify-center">
          <div className="bg-white rounded-lg w-30p p-6 shadow-xl">
            <div className="w-full flex flex-col justify-center text-center items-center">
                <div className="bg-orange rounded-full w-16 h-16 flex items-center justify-center text-white text-lg font-medium">A</div>
                <div className="mt-2">
                    <h1 className="font-medium text-lg">Example</h1>
                    <h1>Example@gmaail.com</h1>
                </div>
            </div>
            <div className="w-full mt-6">
                <Button className="bg-mainblue w-full">Continue as Example</Button>
            </div>
            <div className="flex items-center my-6 justify-center space-x-4">
                <div className="w-full border-b h-1"/>
                <p className="text-sm w-full text-center">Or login with another account</p>
                <div className="w-full border-b h-1"/>
            </div>
            <div className="flex flex-col space-y-2">
                <Input placeholder="jhon@mail.com" label="Email"/>
                <Input placeholder="******" label="Password"/>
            </div>
            <Button className="bg-teal-500 w-full mt-6">Login</Button>
          </div>
      </div>
    );
  }
}
