import React from 'react'
import { AiOutlinePicture } from 'react-icons/ai';
import { FiCamera, FiCloudOff, FiEdit, FiSearch, FiTrash, FiUsers } from 'react-icons/fi';
import Button from '../../../Components/Button';
import Input from '../../../Components/Input';
import Loading from '../../../Components/Loading';
import MyModal from "../../../Components/MyModal"
import MySelect from '../../../Components/MySelect'
import {debounce} from 'lodash';
import Pagination from '../../../Components/Pagination';
import AlertError from '../../../Components/AlertError';

export default class Student extends React.Component {
    state = {
        loading    : false,
        modalCreate: false,
        modalEdit: false,
        alertDelete: false,
        alertStatus: false,
        alertError : false,
        edit       : false,
        filter     : {
            search : ''
        },
        paginate : {
            perPage  : 10,
            page     : 0,
            lastPage : 0,
            pageCount: 0,
            total: 0,
        },
        dataRole : [],
        dataGroup: [],
        dataUsers: [],
        data     :  {
            userId      : '',
            username    : '',
            name        : '',
            mail        : '',
            userPassword: '',
            groupId     : '',
            roleId      : '',
            userTypeId  : 3,
            userDetail: []
        }
    }

    componentDidMount(){
        Promise.all([this.getData(),this.getRole(),this.getGroup()])
    }

    getData = async () => {
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}user?userTypeId=3&search=${this.state.filter.search}&page=${this.state.paginate.page}&size=${this.state.paginate.perPage}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            this.setState({
                dataUsers : response.list,
                alertError : false,
                loading : false,
                paginate : {
                    ...this.state.paginate,
                    page : response.pageable.page,
                    lastPage : response.pageable.totalPages,
                    pageCount:response.pageable.totalPages,
                    total:response.pageable.totalElements,
                }
            })
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                loading : false,
            })
        });
    }

    getRole = async () => {
        return await fetch(`${process.env.REACT_APP_API_URL}master-role`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{

            let roleTemp = []
            response.list.map((e)=>roleTemp.push({label:e.roleName,value:e.id}))

            this.setState({
                dataRole : roleTemp,
                // loading : false,
            })
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                // loading : false,
            })
        });
    }

    getGroup = async () => {
        return await fetch(`${process.env.REACT_APP_API_URL}group`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            let groupTemp = []
            response.list.map((e)=>groupTemp.push({label:e.groupName,value:e.id}))
            this.setState({
                dataGroup : groupTemp,
                // loading : false,
            })
        }).catch(err=>{
            this.setState({ 
                alertError : true,
                // loading : false,
            })
        });
    }

    handleSave = async () => {
        let data = {
            userId      : this.state.data.userId,
            username    : this.state.data.username,
            name        : this.state.data.name,
            mail        : this.state.data.mail,
            userPassword: this.state.data.userPassword,
            groupId     : this.state.data.groupId.value,
            roleId      : this.state.data.roleId.value,
            userTypeId  : 3,
        }
        this.setState({loading:true})
        const method = this.state.edit ? 'PUT':'POST'
        return await fetch(`${process.env.REACT_APP_API_URL}user`, {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
            body: JSON.stringify(data)
        }).then(async()=>{
            await this.getData()
            this.setState({modalCreate:false,loading:false,modalEdit:false,alertError:false})
        }).catch(err=>{
            this.setState({loading:false})
            console.log(err)
        });
    }

    handleInputChange = (e) => {
        this.setState({
            data:{
                ...this.state.data,
                [e.target.name] : e.target.value
            }
        })
    }

    handleFilter = debounce((search) => {
        this.setState({
            filter : {
                search : search
            }
        },()=>{
            this.getData()
        })
    },500)

    handleEdit = async (id) => {
        // console.log(id)
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}user/${id}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(res => res.json())
        .then(response=>{
            this.setState({
                modalEdit : true,
                edit : true,
                loading : false,
                data : {
                    userId      : response.uniqueId,
                    username    : response.username,
                    name        : response.fullName,
                    mail        : response.email,
                    userPassword: '',
                    groupId     : {value:response.userDetail.groupMappings[0].groupId,label:response.userDetail.groupMappings[0].groupName},
                    roleId      : {value:response.userDetail.roleMapping[0].roleId,label:response.userDetail.roleMapping[0].role},
                    userTypeId  : 3,
                    userDetail : response.userDetail
                }
            })
        }).catch(err=>{
            // console.log(err)
            this.setState({loading:false})
        });

    }
    
    hanldeCreate = () => {
        this.setState({
            edit       : false,
            modalCreate: true,
            data       : {
                userId      : '',
                username    : '',
                name        : '',
                mail        : '',
                userPassword: '',
                groupId     : '',
                roleId      : 1,
                userTypeId  : 3,
                userDetail : []
            }
        })
    }

    handlePageChange = (selected) => {
        this.setState({ 
            paginate : {
                ...this.state.paginate,
                page: selected
            }
         }, () => {
          this.getData();
        });
    }; 

    handleDelete = async () =>{
        this.setState({loading:true})
        return await fetch(`${process.env.REACT_APP_API_URL}user/3/${this.state.data.userId}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SSO-TOKEN' : localStorage.getItem('token')
            },
        }).then(()=>{
            this.setState({
                paginate : {
                    ...this.state.paginate,
                    page : 0
                }
            },async()=>{
                await this.getData()
                this.setState({
                    loading:false,
                    alertDelete:false
                })
            })
        }).catch(err=>{
            this.setState({loading:false})
            console.log(err)
        });
    }

    render(){
        return (
            <div className="px-4">
                <Loading visible={this.state.loading}/>
                <div className="flex items-center justify-between space-x-4 flex-1">
                    <h1 className="text-black text-2xl font-medium">Admin</h1>
                    <Button onClick={()=>{this.hanldeCreate()}} className="bg-mainblue">
                        <span>Add Admin</span>
                    </Button>
                </div>
                <div className="my-4 flex items-center justify-between">
                    <div className="flex items-center py-4 space-x-4 text-mainblue">
                        <FiUsers className="text-xl cursor-pointer text-mainblue"/>
                        <span>{this.state.paginate.total} Active Admin</span>
                    </div>
                    <div className="w-20p">
                        <Input name="search" onChange={(e)=>{this.handleFilter(e.target.value)}} icon={<FiSearch/>} placeholder="Search" />
                    </div>
                </div>
                <div className="w-full">
                    <table className="w-full bg-white rounded-lg">
                        <thead className="border-b text-left text-black">
                            <tr>
                                <th className="py-4 px-4">No</th>
                                <th>User ID</th>
                                <th>Username</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Group</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.dataUsers.length === 0 ? (
                                    <tr>
                                        <td colSpan="9" className="py-8 px-4">
                                            <div className="flex items-center space-x-4 justify-center w-full text-center text-gray">
                                                <FiCloudOff/>
                                                <p>No data</p>
                                            </div>
                                        </td>
                                    </tr>
                                ) :(
                                    this.state.dataUsers.map((user,key)=>{
                                        return(
                                            <tr key={key} className="hover:bg-table odd:bg-table">
                                                <td className="py-3 px-4">{key+1}</td>
                                                <td>{user.userUniqueId}</td>
                                                <td>{user.userCred.username}</td>
                                                <td>{user.userCred.fullName}</td>
                                                <td>{user.userCred.email}</td>
                                                <td>{user.groups[0].groupName}</td>
                                                <td>{user.roles[0].role}</td>
                                                <td>{user.status === 1 ? 'Active' : 'Non-Active'}</td>
                                                <td>
                                                    <div className="flex items-center space-x-2">
                                                        <FiEdit onClick={()=>{this.handleEdit(user.userUniqueId)}} className="cursor-pointer"/>
                                                        <FiTrash onClick={()=>{this.setState({alertDelete:true,data:{...this.state.data,userId:user.userUniqueId}})}} className="cursor-pointer"/>
                                                    </div>
                                                </td>
                                            </tr>
                                        )
                                    })
                                    
                                )
                            }
                            
                            
                            
                        </tbody>
                    </table>
                </div>
                <Pagination 
                    onPageChange={(selected)=>{this.handlePageChange(selected)}} 
                    lastPage={this.state.paginate.lastPage} 
                    page={this.state.paginate.page} 
                    pageCount={this.state.paginate.pageCount} 
                />
                <AlertError show={this.state.alertError} close={()=>{this.setState({alertError:false})}} OnRefresh={()=>{this.state.modalCreate === true || this.state.modalEdit === true ? this.handleSave() : this.getData()}} />
                <MyModal lg modalIsOpen={this.state.modalEdit} setModal={()=>{this.setState({modalEdit:false})}}>
                    <div className="flex justify-center items-center font-Poppins">
                        <h1 className="text-xl font-bold text-center">{this.state.edit ? 'Admin Detail' :'Add New Admin'}</h1>
                    </div>
                    <form onSubmit={(e)=>{e.preventDefault()}}>
                    <div className="flex mt-8">
                        <div className="rounded w-48 h-40 bg-gray overflow-hidden   flex items-center justify-center relative group">
                            <AiOutlinePicture className="text-6xl"/>
                            <div className="absolute px-2 flex items-center justify-center bg-gray h-full w-full inset-0 opacity-0 group-hover:opacity-100 transition duration-300 ease-in-out">
                                <button className="px-2 flex flex-col items-center justify-center focus:outline-none ">
                                    <FiCamera className="text-xl text-black"/>
                                    <span className="leading-none text-sm mt-2">Change Picture</span>
                                    <p className="text-xs">Max. File size : 2 Mb</p>
                                </button>
                            </div>
                        </div>
                        <div className=" w-full  ml-12">
                            <div className="grid grid-cols-3 gap-5">
                                <Input label="UserId" readOnly name="userId" onChange={(e)=>{this.handleInputChange(e)}} value={this.state.data.userId} placeholder="UserId"/>
                                <Input label="Username" name="username" onChange={(e)=>{this.handleInputChange(e)}} value={this.state.data.username} placeholder="Username"/>
                                <Input label="Full Name" name="name" onChange={(e)=>{this.handleInputChange(e)}} value={this.state.data.name} placeholder="Full Name"/>
                                <Input label="Nomor Identitas (NIK)" readOnly value={this.state.data.userDetail.nik} name="nik" placeholder="Nomor Identitas (NIK)"/>
                                <Input label="Tempat Lahir" readOnly name="tempat_lahir" value={this.state.data.userDetail.birthPlace} placeholder="Tempat Lahir"/>
                                <Input label="Tanggal Lahir" readOnly name="tgl_lahir" value={this.state.data.userDetail.birthDate} placeholder="Tanggal Lahir"/>
                                <Input label="Jenis Kelamin" readOnly name="jk" value={this.state.data.userDetail.gender} placeholder="Jenis Kelamin"/>
                                <Input label="Kewarganegaraan" readOnly name="kewarganegaraan" value={this.state.data.userDetail.nationality} placeholder="Kewarganegaraan"/>
                                <Input label="Agama" readOnly name="religion" value={this.state.data.userDetail.religionId} placeholder="Agama"/>
                                <Input label="Nama Ibu Kandung" readOnly name="nama_ibu_kandung"  value={this.state.data.userDetail.biologicalMotherName} placeholder="Nama Ibu Kandung"/>
                                <Input label="Alamat Email" name="mail" onChange={(e)=>{this.handleInputChange(e)}} value={this.state.data.mail} placeholder="Alamat Email"/>
                                <Input label="No. Telp" readOnly name="telp" value={this.state.data.userDetail.phoneNumber} placeholder="No. Telp"/>
                                <div className="col-span-3 grid grid-cols-2 gap-5">
                                    <MySelect data={this.state.dataGroup} value={this.state.data.groupId} onChange={(e,a)=> a.action === "clear"?this.setState({data:{...this.state.data,groupId:null}}): this.setState({data:{...this.state.data,groupId:{value:e.value,label:e.label}}})} label="Group" placeholder="Group"/>
                                    <MySelect data={this.state.dataRole} value={this.state.data.roleId} onChange={(e,a)=> a.action === "clear"?this.setState({data:{...this.state.data,roleId:null}}): this.setState({data:{...this.state.data,roleId:{value:e.value,label:e.label}}})} label="Role" placeholder="Role"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="flex justify-center mt-12">
                        <Button onClick={()=>this.handleSave()} className="bg-mainblue">Save</Button>
                    </div>
                    </form>
                </MyModal>
                <MyModal md modalIsOpen={this.state.alertDelete} setModal={()=>{this.setState({alertDelete:false})}}>
                    <div className="flex justify-center flex-col items-center font-Poppins">
                        <h1 className="text-xl font-bold">Are you sure ?</h1>
                        <p>Do you want to delete this Student?</p>
                        <div className="flex items-center space-x-2 mt-8">
                            <button onClick={()=>{this.handleDelete()}} className="p-4 focus:outline-none w-24 bg-red text-white rounded" >Yes</button>
                            <button onClick={()=>{this.setState({alertDelete:false})}} className="p-4 focus:outline-none w-24 border text-black rounded" >Cancel</button>
                        </div>
                    </div>
                </MyModal>
                <MyModal lg modalIsOpen={this.state.modalCreate} setModal={()=>{this.setState({modalCreate:false})}}>
                    <div className="flex justify-center items-center font-Poppins">
                        <h1 className="text-xl font-bold text-center">Add New Admin</h1>
                    </div>
                    <form  onSubmit={(e)=>{e.preventDefault();this.handleSave()}}>
                        <div className="grid grid-cols-2 gap-5 mt-8">
                            <div className="col-span-2 grid-cols-3 grid gap-5">
                                <Input label="UserId" onChange={(e)=>{this.handleInputChange(e)}} name="userId" placeholder="UserId"/>
                                <Input label="Username" onChange={(e)=>{this.handleInputChange(e)}} name="username" placeholder="Username"/>
                                <Input label="Full Name" onChange={(e)=>{this.handleInputChange(e)}} name="name" placeholder="Full Name"/>
                            </div>
                            <Input label="Email" onChange={(e)=>{this.handleInputChange(e)}} name="mail" placeholder="Email"/>
                            <Input label="Password" onChange={(e)=>{this.handleInputChange(e)}} name="userPassword" placeholder="********"/>
                                <MySelect data={this.state.dataGroup} value={this.state.data.groupId} onChange={(e,a)=> a.action === "clear"?this.setState({data:{...this.state.data,groupId:null}}): this.setState({data:{...this.state.data,groupId:{value:e.value,label:e.label}}})} label="Group" placeholder="Group"/>
                                <MySelect data={this.state.dataRole} value={this.state.data.roleId} onChange={(e,a)=> a.action === "clear"?this.setState({data:{...this.state.data,roleId:null}}): this.setState({data:{...this.state.data,roleId:{value:e.value,label:e.label}}})} label="Role" placeholder="Role"/>
                        </div>
                        <div className="flex justify-center mt-8">
                            <Button className="bg-mainblue">Save</Button>
                        </div>
                    </form>
                </MyModal>
            </div>
        )

    }
}
