import React from 'react'
import { FiCalendar, FiGrid, FiSearch } from 'react-icons/fi';
import Loading from '../../Components/Loading';
import Pagination from '../../Components/Pagination';
import {debounce} from 'lodash';
const axios = require('axios');
var dateFormat = require('dateformat');

export default class Report extends React.Component {
    
    state = {
        loading    : false,
        alertError : false,
        filter     : {
            search : ''
        },
        paginate : {
            perPage  : 10,
            page     : 0,
            lastPage : 0,
            pageCount: 0,
            total: 0,
        },
        dataReports : [],
    }

    componentDidMount(){
        Promise.all([this.getData()])
    }

    getData = async () => {
        this.setState({loading:true})
        await axios.get(`${process.env.REACT_APP_API_URL}log-report?search=${this.state.filter.search}&page=${this.state.paginate.page}&size=${this.state.paginate.perPage}`,
        {
            headers: {
                'SSO-TOKEN' : localStorage.getItem('token')
            }
        })
        .then((response)=> {
            this.setState({
                dataReports : response.data.list,
                paginate : {
                    ...this.state.paginate,
                    page : response.data.pageable.page,
                    lastPage : response.data.pageable.totalPages,
                    pageCount:response.data.pageable.totalPages,
                    total:response.data.pageable.totalElements,
                }
            })
        })
        .catch((error)=>{
            console.log(error)
        }).then(()=>{
            this.setState({loading:false})
        });
    }
    
    handlePageChange = (selected) => {
        this.setState({ 
            paginate : {
                ...this.state.paginate,
                page: selected
            }
         }, () => {
          this.getData();
        });
    };

    handleFilter = debounce((search) => {
        this.setState({
            filter : {
                search : search
            }
        },()=>{
            this.getData()
        })
    },500)

    render(){
        return (
            <div className="px-4">
                 <Loading visible={this.state.loading}/>
                <h1 className="text-black text-2xl font-medium">Report</h1>
                <div className="my-4 bg-white rounded py-2 px-4 flex items-center justify-between">
                    <div className="flex items-center space-x-4 flex-1">
                        <div className="flex items-center py-4 space-x-2 w-48 border rounded px-4">
                            <FiGrid className="text-gray text-xl cursor-pointer"/>
                            <input type="text" className="w-full h-full focus:outline-none" placeholder="Select Apps"/>
                        </div>
                        <div className="flex items-center py-4 space-x-2 w-48 border rounded px-4">
                            <FiCalendar className="text-gray text-xl cursor-pointer"/>
                            <input type="text" className="w-full h-full focus:outline-none" placeholder="Date"/>
                        </div>
                        <div className="flex items-center py-4 space-x-2 w-30p border rounded px-4">
                            <FiSearch className="text-gray text-xl cursor-pointer"/>
                            <input onChange={(e)=>{this.handleFilter(e.target.value)}} type="text" className="w-full h-full focus:outline-none" placeholder="Search"/>
                        </div>
                    </div>
                    <button style={{backgroundColor:'#20744B'}} className="py-4 focus:outline-none rounded px-6 flex items-center space-x-2 text-white">
                        <svg width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M18.1726 1.70771H11.2992V0L0 1.83833V17.9491L11.2992 19.9999V17.4702H18.1726C18.6296 17.4702 19 17.1193 19 16.6864V2.49148C19 2.05864 18.6296 1.70771 18.1726 1.70771ZM7.85915 13.4468L6.3198 13.3372L5.27975 10.4592L4.23964 13.2276L2.82517 13.096L4.4685 9.76095L2.94991 6.18989L4.4061 6.10224L5.32134 8.75505L6.40303 5.99258L7.85915 5.90486L6.09101 9.67114L7.85915 13.4468ZM18.2813 16.817H11.2759L11.2645 15.4658H12.9518V13.8924H11.2513L11.2434 12.9621H12.9518V11.3887H11.2301L11.2223 10.4584H12.9518V8.88502H11.2166V7.95468H12.9518V6.38133H11.2166V5.45099H12.9518V3.87764H11.2166V2.44811H18.2813V16.817Z" fill="white"/>
                            <path d="M16.8351 3.87769H13.9019V5.45104H16.8351V3.87769Z" fill="white"/>
                            <path d="M16.8351 6.38147H13.9019V7.95482H16.8351V6.38147Z" fill="white"/>
                            <path d="M16.8351 8.88513H13.9019V10.4585H16.8351V8.88513Z" fill="white"/>
                            <path d="M16.8351 11.3888H13.9019V12.9621H16.8351V11.3888Z" fill="white"/>
                            <path d="M16.8351 13.8925H13.9019V15.4658H16.8351V13.8925Z" fill="white"/>
                        </svg>
                        <span>Download</span>
                    </button>
                </div>
                <div className="w-full">
                    <table className="w-full bg-white rounded-lg">
                        <thead className="border-b text-left text-black">
                           <tr>
                            <th className="py-4 px-4">No</th>
                            <th>Email</th>
                            <th>Full Name</th>
                            <th>App URL</th>
                            <th>HTTP Code</th>
                            <th>HTTP Status</th>
                            <th>Date</th>
                            <th>Time</th>
                           </tr>
                        </thead>
                            <tbody>
                            {this.state.dataReports.map((e,i)=>{
                                return(
                                    <tr key={i} className="hover:bg-table odd:bg-table">
                                        <td className="py-3 px-4">{i +1}</td>
                                        <td>{e.email}</td>
                                        <td>{e.fullName}</td>
                                        <td>{e.app_url}</td>
                                        <td>{e.httpCode}</td>
                                        <td>{e.httpStatus}</td>
                                        <td>{dateFormat(new Date(e.date),"dd-mm-yyyy")}</td>
                                        <td>{dateFormat(new Date(e.date),"hh:MM:ss")}</td>
                                    </tr>

                                )
                            })}
                        </tbody>
                    </table>
                </div>
                <Pagination 
                    onPageChange={(selected)=>{this.handlePageChange(selected)}} 
                    lastPage={this.state.paginate.lastPage} 
                    page={this.state.paginate.page} 
                    pageCount={this.state.paginate.pageCount} 
                />
            </div>
        )
    }
}
