import React from 'react'
import MyModal from './MyModal'

export default function AlertError({show,close,OnRefresh}) {
    return (
        <MyModal isAlert md modalIsOpen={show} setModal={()=>{close()}}>
            <div className="flex justify-center flex-col items-center font-Poppins">
                <h1 className="text-xl font-bold text-red">Oppps!</h1>
                <span>Something when wrong. Keep calm and try again.</span>
                <div className="flex items-center space-x-2 mt-2">
                    <button onClick={()=>{OnRefresh()}} className="p-4 focus:outline-none w-48 bg-red text-white rounded" >Try Again</button>
                    <button onClick={()=>{close()}} className="p-4 focus:outline-none w-48 border text-black rounded">Cancel</button>
                </div>
            </div>
        </MyModal>
    )
}
