import React from 'react'
import { Link } from 'react-router-dom'
import Button from "../Components/Button"

export default function CheckYourEmail() {

    return (
        <div className="relative">
            <div style={{backgroundColor:'#F5F6FA'}} className=" h-screen  overflow-hidden relative">
                <img src={require('../assets/img/loginbottom.svg')} alt="loginbottom" className="absolute bottom-0 w-full -mb-24 lg:-mb-40 -sm:hidden"/>
                <div className="relative z-0 flex flex-col justify-center items-center mt-48 px-4 md:px-8">
                    <img src={require('../assets/img/logo.png')} alt="logo"/>
                    <h1 style={{color:'#575757'}} className="font-light text-2xl  text-center mt-2">Check In Your Mail !</h1>
                    <p className="text-gray w-full md:w-1/2 lg:w-1/3 xl:w-1/4 text-center text-sm">We just emailed you with the intructions to reset your password</p>
                    <Link className="flex w-full justify-center" to="/login">
                        <Button className="bg-mainblue mt-6 w-full md:w-1/2 lg:w-1/3 xl:w-1/4">Done</Button>
                    </Link>
                </div>
            </div>
        </div>
    )
}
