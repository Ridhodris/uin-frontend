import React, { useState } from 'react'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers';
import * as yup from "yup";
import { Link, useHistory, useParams } from "react-router-dom";
import Button from "../Components/Button"
import Loading from '../Components/Loading';
import Swal from 'sweetalert2'

const axios = require('axios');

export default function ResetPassword() {
    let { token } = useParams();
    let history = useHistory();



    const schema = yup.object().shape({
        newPassword: yup.string().required().min(6).label('Password'),
        confirmPassword: yup.string().required().min(6).oneOf([yup.ref('newPassword'), null],"Passwords must match").label('Confirm Password'),
    });
    const [loading, setLoading] = useState(false)


    const { register, handleSubmit, errors } = useForm({
        resolver: yupResolver(schema)
    });


    const onSubmit = (data) => {
        setLoading(true)
        axios.put(`${process.env.REACT_APP_API_URL}forgot-password/${token}`, {
            userPassword: data.newPassword,
        })
        .then((response)=> {
            // localStorage.setItem('token',response.data.rowData)
            // history.push('/checkyouremail');
            Swal.fire({
                title: 'Successful password reset !',
                text: "You can now use your new password to log in to your account",
                icon: 'success',
                showCancelButton: false,
                confirmButtonColor: '#029CC3',
                confirmButtonText: 'Done'
              }).then((result) => {
                if (result.isConfirmed) {
                    return history.push('/login');
                }
              })
        })
        .catch((error)=>{
            // console.log()
            Swal.fire(
                'Oopss!',
                error.response.data.message,
                'error'
              )
        }).then(()=>{
            setLoading(false)
        });
    }

    return (
        <div className="relative">
        <Loading visible={loading}/>
            <div style={{backgroundColor:'#F5F6FA'}} className=" h-screen  overflow-hidden relative">
                <img src={require('../assets/img/loginbottom.svg')} alt="loginbottom" className="absolute bottom-0 w-full -mb-24 lg:-mb-40 -sm:hidden"/>
                <div className="relative z-0 flex flex-col justify-center items-center mt-48 px-4 md:px-8">
                    <img src={require('../assets/img/logo.png')} alt="logo"/>
                    <h1 style={{color:'#575757'}} className="font-light text-2xl  text-center mt-2">Reset your password</h1>
                    <p className="text-gray w-full md:w-1/2 lg:w-1/3 xl:w-1/4 text-center text-sm">Almost done. Enter your new password, and you're good to go.</p>
                    <form onSubmit={handleSubmit(onSubmit)} className="w-full flex flex-col mt-5 justify-center items-center">
                        {/* <input style={{color:'#686868'}} placeholder="email@uin.com" type="text" className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4 focus:outline-none px-4 py-4 border-b"/> */}
                        <div className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4">
                            <input ref={register} type="password" id="newPassword" name="newPassword" placeholder="Enter a new password" className={`w-full border-b px-4 py-4 focus:outline-none ${errors.newPassword ? "border border-red" : ''}`}/>
                            <p className={`text-xs text-red ${errors.newPassword ? "mb-2":""}`} >{errors.newPassword?.message}</p>
                        </div>
                        <div className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4">
                            <input ref={register} type="password" id="confirmPassword" name="confirmPassword" placeholder="Confirm your new password" className={`w-full px-4 py-4 focus:outline-none ${errors.confirmPassword ? "border border-red" : ''}`}/>
                            <p className={`text-xs text-red ${errors.confirmPassword ? "mb-2":""}`} >{errors.confirmPassword?.message}</p>
                        </div>
                        <Button className="bg-mainblue mt-6 w-full md:w-1/2 lg:w-1/3 xl:w-1/4">Reset Password</Button>
                        <p className="text-sm text-gray mt-6">Did you remember your password? <Link className="text-mainblue hover:underline cursor-pointer" to="/login">Try Sign In</Link></p>
                    </form>
                </div>
            </div>
        </div>
    )
}
