import React from 'react'
import Modal from 'react-modal';
const customStyles = {     
      top         : '50%',
      left        : '50%',
      right       : 'auto',
      bottom      : 'auto',
      marginRight : '-50%',
      transform   : 'translate(-50%, -50%)',
      padding     : '34px',
      border      : 'none',
      boxShadow   : ' 0px 15px 46px rgba(14, 13, 22, 0.185779)',
      borderEadius: '25px',
      maxHeight : '90vh',
  };
export default function MyModal({modalIsOpen,setModal,children,md,lg,full,isAlert }) {

  let width = '50%'
  if (md) {
    width = '30%'
  }else if(full){
    width = '90%'
  }else if(lg){
    width = '70%'
  }
    return (
        <Modal
            appElement={document.getElementById('root')}
            isOpen={modalIsOpen}
            onRequestClose={setModal}
            contentLabel="Example Modal"
            style={{
              content :{
                ...customStyles,
                width: width
              },
              overlay: {zIndex: isAlert ? 41 : 40}
            }}
            >
            {children }
        </Modal>
    )
}
